﻿using System.Windows;

namespace TestProviderUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            new StartUp();
        }
    }
}