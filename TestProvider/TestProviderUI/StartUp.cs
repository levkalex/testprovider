﻿using Autofac;
using AutoMapper;
using Common.Entities;
using CoreTestProvider.Common;
using CoreTestProvider.Common.Interfaces;
using Database;
using Database.Common;
using System;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;
using Common.Models;
using TestProviderUI.Models;
using TestProviderUI.Utils.Extensions;
using TestProviderUI.View;
using TestProviderUI.Core;
using CoreTestProvider;
using Microsoft.Extensions.Configuration;
using Common.Constants;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;

namespace TestProviderUI
{
    public class StartUp
    {
        public static IContainer CompositionRoot()
        {
            var builder = new ContainerBuilder();
            var executingAssembly = Assembly.GetExecutingAssembly();
            var allFormTypes = executingAssembly.GetTypes().Where(x => x.IsClass && x.IsSubclassOf(typeof(UserControl)));
            var allViewModelsTypes = executingAssembly.GetTypesImplementingGenericType(typeof(IViewModel<>));

            builder.RegisterTypes(allFormTypes.ToArray()).InstancePerLifetimeScope();
            builder.RegisterTypes(allViewModelsTypes.ToArray()).InstancePerLifetimeScope();
            builder.Register(ctx => ctx.Resolve<LoginPage>()).As<IPasswordProvider>().InstancePerLifetimeScope();
            
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();

            var allQueryHandlerTypes = loadedAssemblies.Select(x=> x.GetTypesImplementingGenericType(typeof(IQueryHandler<,>)));
            var allCommandHandlerTypes = loadedAssemblies.Select(x => x.GetTypesImplementingGenericType(typeof(ICommandHandler<,>)));
            var allCommandHandlerTypesWithoutReturnType = loadedAssemblies.Select(x => x.GetTypesImplementingGenericType(typeof(ICommandHandler<>)));

            builder.RegisterTypes(allQueryHandlerTypes.SelectMany(x=>x).ToArray());
            builder.RegisterTypes(allCommandHandlerTypes.SelectMany(x => x).ToArray());
            builder.RegisterTypes(allCommandHandlerTypesWithoutReturnType.SelectMany(x => x).ToArray());

            builder.RegisterType<Redirect>().As<IRedirect>().SingleInstance();
            builder.RegisterType<UserContext>().As<IUserContext>().SingleInstance();
            builder.RegisterType<Dispatcher>().As<IDispatcher>();

            /*Database*/
            builder.Register(x => new UnitOfWork(new DbContextOptionsFactory().CreateDbContext(null))).As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.Register(x => new ConfigurationBuilder().AddJsonFile("appsettings.json", true, true).Build()).As<IConfiguration>().SingleInstance();
            
            /*Mapper*/
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddCoreMapperConfig();

                cfg.CreateMap<Answer, BaseAnswerViewModel>().ReverseMap();
                cfg.CreateMap<Question, BaseQuestionViewModel<BaseAnswerViewModel>>();
                cfg.CreateMap<Answer, TestAnswerViewModel>();
                cfg.CreateMap<Question, TestQuestionViewModel>();
                cfg.CreateMap<PassedTestAnswerModel, TestAnswerViewModel>().ReverseMap();
                cfg.CreateMap<PassedTestQuestionModel, TestQuestionViewModel>();
            });
            builder.Register(x => config.CreateMapper()).As<IMapper>();

            return builder.Build();
        }
        public StartUp()
        {
            var container = CompositionRoot();
            
            var redirect = container.Resolve<IRedirect>();
            redirect.To<LoginPage>("MainPage");
        }
    }
}
