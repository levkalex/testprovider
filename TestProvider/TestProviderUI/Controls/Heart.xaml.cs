﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using TestProviderUI.Annotations;

namespace TestProviderUI.Controls
{
    /// <summary>
    /// Interaction logic for Heart.xaml
    /// </summary>
    public partial class Heart : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public string Source => IsEnable ? "/images/fill-like.png" : "/images/empty-like.png";
        public bool IsEnable { get; set; }

        public Heart()
        {
            InitializeComponent();
        }

        private void Click(object sender, EventArgs e)
        {
            IsEnable = !IsEnable;
            OnPropertyChanged(nameof(Source));

            if (IsEnable == false)
            {
                var sb2 = new Storyboard();

                var ta4 = new DoubleAnimation();
                ta4.SetValue(Storyboard.TargetNameProperty, "FillHeart");
                ta4.BeginTime = new TimeSpan(0);
                ta4.From = FillHeart.ActualWidth;
                ta4.To = FillHeart.ActualWidth - FillHeart.ActualWidth / 8;
                ta4.Duration = new Duration(TimeSpan.FromMilliseconds(150));
                Storyboard.SetTargetProperty(ta4, new PropertyPath(Canvas.WidthProperty));
                sb2.Children.Add(ta4);

                var ta5 = new DoubleAnimation();
                ta5.SetValue(Storyboard.TargetNameProperty, "FillHeart");
                ta5.BeginTime = new TimeSpan(0);
                ta5.From = FillHeart.ActualHeight;
                ta5.To = FillHeart.ActualHeight - FillHeart.ActualHeight / 8;
                ta5.Duration = new Duration(TimeSpan.FromMilliseconds(150));
                Storyboard.SetTargetProperty(ta5, new PropertyPath(Canvas.HeightProperty));
                sb2.Children.Add(ta5);

                var ta6 = new DoubleAnimation();
                ta6.SetValue(Storyboard.TargetNameProperty, "FillHeart");
                ta6.From = FillHeart.ActualWidth - FillHeart.ActualWidth / 8;
                ta6.To = FillHeart.ActualWidth;
                ta6.BeginTime = TimeSpan.FromMilliseconds(165);
                ta6.Duration = new Duration(TimeSpan.FromMilliseconds(120));
                Storyboard.SetTargetProperty(ta6, new PropertyPath(Canvas.WidthProperty));
                sb2.Children.Add(ta6);

                var ta7 = new DoubleAnimation();
                ta7.SetValue(Storyboard.TargetNameProperty, "FillHeart");
                ta7.From = FillHeart.ActualHeight - FillHeart.ActualHeight / 8;
                ta7.To = FillHeart.ActualHeight;
                ta7.BeginTime = TimeSpan.FromMilliseconds(165);
                ta7.Duration = new Duration(TimeSpan.FromMilliseconds(120));
                Storyboard.SetTargetProperty(ta7, new PropertyPath(Canvas.HeightProperty));
                sb2.Children.Add(ta7);

                sb2.Completed += (object? sender, EventArgs e) =>
                {
                    FillHeart.BeginAnimation(Grid.WidthProperty, null);
                    FillHeart.BeginAnimation(Grid.HeightProperty, null);
                };
                sb2.Begin(this);
            }
            else
            {
                var sb2 = new Storyboard();

                var ta4 = new DoubleAnimation();
                ta4.SetValue(Storyboard.TargetNameProperty, "FillHeart");
                ta4.BeginTime = new TimeSpan(0);
                ta4.From = FillHeart.ActualWidth;
                ta4.To = FillHeart.ActualWidth + FillHeart.ActualWidth / 8;
                ta4.Duration = new Duration(TimeSpan.FromMilliseconds(150));
                Storyboard.SetTargetProperty(ta4, new PropertyPath(Canvas.WidthProperty));
                sb2.Children.Add(ta4);

                var ta5 = new DoubleAnimation();
                ta5.SetValue(Storyboard.TargetNameProperty, "FillHeart");
                ta5.BeginTime = new TimeSpan(0);
                ta5.From = FillHeart.ActualHeight;
                ta5.To = FillHeart.ActualHeight + FillHeart.ActualHeight / 8;
                ta5.Duration = new Duration(TimeSpan.FromMilliseconds(150));
                Storyboard.SetTargetProperty(ta5, new PropertyPath(Canvas.HeightProperty));
                sb2.Children.Add(ta5);

                var ta6 = new DoubleAnimation();
                ta6.SetValue(Storyboard.TargetNameProperty, "FillHeart");
                ta6.From = FillHeart.ActualWidth + FillHeart.ActualWidth / 8;
                ta6.To = FillHeart.ActualWidth;
                ta6.BeginTime = TimeSpan.FromMilliseconds(165);
                ta6.Duration = new Duration(TimeSpan.FromMilliseconds(120));
                Storyboard.SetTargetProperty(ta6, new PropertyPath(Canvas.WidthProperty));
                sb2.Children.Add(ta6);

                var ta7 = new DoubleAnimation();
                ta7.SetValue(Storyboard.TargetNameProperty, "FillHeart");
                ta7.From = FillHeart.ActualHeight + FillHeart.ActualHeight / 8;
                ta7.To = FillHeart.ActualHeight;
                ta7.BeginTime = TimeSpan.FromMilliseconds(165);
                ta7.Duration = new Duration(TimeSpan.FromMilliseconds(120));
                Storyboard.SetTargetProperty(ta7, new PropertyPath(Canvas.HeightProperty));
                sb2.Children.Add(ta7);

                sb2.Completed += (object? sender, EventArgs e) =>
                {
                    FillHeart.BeginAnimation(Grid.WidthProperty, null);
                    FillHeart.BeginAnimation(Grid.HeightProperty, null);
                };
                sb2.Begin(this);
            }
        }


        private void FillHeart_SizeChanged(object sender, SizeChangedEventArgs e)
        {
        }
    }
}
