﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestProviderUI
{
    public class Like : Control
    {
        public int IsChecked { get; set; }

        static Like()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Like), new FrameworkPropertyMetadata(typeof(Like)));          
        }

        public Like()
        {
            MouseMove += MouseEnterHandler;
        }

        private void MouseEnterHandler(object sender, RoutedEventArgs e)
        {
            var heart = (Image)Template.FindName("heart", this);

            DoubleAnimation buttonAnimation = new DoubleAnimation();
            buttonAnimation.From = heart.ActualWidth;
            buttonAnimation.To = heart.ActualWidth + 20;
            buttonAnimation.Duration = TimeSpan.FromSeconds(3);
            heart.BeginAnimation(Button.WidthProperty, buttonAnimation);
            buttonAnimation.AutoReverse = true;


        }
    }
}
