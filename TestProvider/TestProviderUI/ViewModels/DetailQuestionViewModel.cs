﻿using AutoMapper;
using Common.Entities;
using CoreTestProvider.Common;
using CoreTestProvider.Queries;
using System.Threading.Tasks;
using System.Windows.Input;
using TestProviderUI.Core;
using TestProviderUI.Utils;
using TestProviderUI.View;

namespace TestProviderUI.VIewModels
{
	public class DetailQuestionViewModel : BaseViewModel, IViewModel<DetailQuestionPage>
    {
        public Question Question { get; set; }
        private IDispatcher _dispatcher;
        private IRedirect _redirect;
        private int _id;
        public DetailQuestionViewModel(IDispatcher dispatcher, IMapper mapper, IRedirect redirect, int id)
        {
            _dispatcher = dispatcher;
            _redirect = redirect;
            _id = id;
        }

        public ICommand FormLoadedCommand => new RelayAsyncCommand(FormLoaded);
        public ICommand BackToMainMenuCommand => new RelayCommand(BackToMainMenu);
        private async Task FormLoaded(object o)
        {
            _redirect.LockUI();
            Question = await _dispatcher.Dispatch<Question>(new QuestionByIdFilterQuery { Id = _id });
            OnPropertyChanged(nameof(Question));
            _redirect.UnlockUI();
        }
        private void BackToMainMenu(object index)
        {
            _redirect.To<QuestionsPage>("All questions");
        }
    }
}
