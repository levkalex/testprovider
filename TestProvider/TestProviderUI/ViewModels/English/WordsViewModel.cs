﻿using Common.Entities;
using CoreTestProvider.Common;
using CoreTestProvider.Queries;
using Database.Common;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using TestProviderUI.Core;
using TestProviderUI.Models;
using TestProviderUI.Utils;
using TestProviderUI.View;
using TestProviderUI.View.English;

namespace TestProviderUI.ViewModels.English
{
    public class WordsViewModel : BaseViewModel, IViewModel<WordsPage>
    {
		private IDispatcher _dispatcher;
		private IRedirect _redirect;
		private IUnitOfWork _unitOfWord;
		public WordsViewModel(IDispatcher dispatcher, IRedirect redirect, IUnitOfWork unitOfWord)
		{
			_dispatcher = dispatcher;
			_redirect = redirect;
			_unitOfWord = unitOfWord;
		}
		public ICommand FormLoadedCommand => new RelayAsyncCommand(FormLoaded);
		public ICommand BackToMainMenuCommand => new RelayCommand(BackToMainMenu);

		public ObservableCollection<Word> Words { get; set; }
		public int Count => Words != null ? Words.Count : 0;
		private async Task FormLoaded(object o)
		{
			_redirect.LockUI();

			//TODO: Replace in command
			var words = await _unitOfWord.WordRepository.FetchAllAsync();
			Words = new ObservableCollection<Word>(words);

			OnPropertyChanged(nameof(Words));
			OnPropertyChanged(nameof(Count));
			_redirect.UnlockUI();
		}

		private Image ByteArrayToImage(byte[] bytesArr)
		{
			using (MemoryStream memstr = new MemoryStream(bytesArr))
			{
				Image img = Image.FromStream(memstr);
				return img;
			}
		}

		private void BackToMainMenu(object index)
		{
			_redirect.To<MainPage>("Main menu");
		}

		public RelayCommand RedirectToCreateWordCommand =>
			new RelayCommand((object obj) => { _redirect.To<CreateWordPage>(); });
	}
}
