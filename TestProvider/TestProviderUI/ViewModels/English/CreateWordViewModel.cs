﻿using Common.Entities;
using Database.Common;
using Microsoft.Win32;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TestProviderUI.Core;
using TestProviderUI.Utils;
using TestProviderUI.View;
using TestProviderUI.View.English;

namespace TestProviderUI.ViewModels.English
{
    public class CreateWordViewModel : BaseViewModel, IViewModel<CreateWordPage>
    {
        #region Binding property
        public string ImagePath { get; set; }

        public Word Word { get; set; }
        #endregion

        #region Property
        private IRedirect _redirect;
        private IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public CreateWordViewModel(IRedirect redirect, IUnitOfWork unitOfWork)
        {
            _redirect = redirect;
            _unitOfWork = unitOfWork;
            Word = new Word();
        }
        #endregion

        #region Commands
        public ICommand SelectImageCommand => new RelayCommand(SelectImage);
        public ICommand BackCommand => new RelayCommand(Back);
        public ICommand SaveCommand => new RelayAsyncCommand(Save);
        #endregion

        #region CommandHandlers
        private void SelectImage(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.FileName = string.Empty;
            openFileDialog.Filter = "Portable Network Graphic (*.png)|*.png";
            if (openFileDialog.ShowDialog() == true)
            {
                ImagePath = openFileDialog.FileName;
                var image = Image.FromFile(ImagePath);

                using (var ms = new MemoryStream())
                {
                    image.Save(ms, image.RawFormat);
                    Word.Image = ms.ToArray();
                    OnPropertyChanged(nameof(Word.Image));
                    OnPropertyChanged(nameof(ImagePath));
                }
            }
        }

        private async Task Save(object parameter)
        {
            _redirect.LockUI();

            //TODO: Replace in command
            await _unitOfWork.WordRepository.AddAsync(Word);
            await _unitOfWork.SaveChangesAsync();

            _redirect.UnlockUI();
            MessageBox.Show("Created!");
            _redirect.To<WordsPage>();
        }

        private void Back(object parameter)
        {
            _redirect.To<WordsPage>();
        }
        #endregion
    }
}
