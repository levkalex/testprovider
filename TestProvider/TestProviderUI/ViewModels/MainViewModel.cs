﻿using TestProviderUI.Utils;
using TestProviderUI.View;
using TestProviderUI.Core;
using System.Threading.Tasks;
using CoreTestProvider.Common;
using TestProviderUI.View.English;

namespace TestProviderUI.VIewModels
{
    public class MainViewModel : BaseViewModel, IViewModel<MainPage>
    {
        private IRedirect _redirect;
        private IUserContext _userContext;
        public string HelloText { get; }
        public MainViewModel(IRedirect redirect, IUserContext userContext)
        {
            _redirect = redirect;
            _userContext = userContext;

            HelloText = $"Приветствуем, {_userContext.Name}";
        }

        public RelayAsyncCommand LogOutCommand => new RelayAsyncCommand(LogOut);

        public async Task LogOut(object o)
        {
            await _userContext.SignOutAsync();
            _redirect.To<LoginPage>("Login");
        }

        public RelayCommand RedirectToQuestionsWindow => 
            new RelayCommand((object obj) => { _redirect.To<TestPage>("Test"); });

        public RelayCommand RedirectToAllQuestionsWindow =>
            new RelayCommand((object obj) => { _redirect.To<QuestionsPage>("Ovveride default text"); });

        public RelayCommand RedirectToHistoryWindow => 
            new RelayCommand((object obj) => { _redirect.To<PassedTestsPage>(title: "History view"); });
        
        public RelayCommand RedirectToAddQuestionWindow =>
            new RelayCommand((object obj) => { _redirect.To<CreateQuestionPage>(); });

        public RelayCommand RedirectToWordCardPage =>
            new RelayCommand((object obj) => { _redirect.To<WordCardPage>(); });

        public RelayCommand RedirectToUsersPage =>
            new RelayCommand((object obj) => { _redirect.To<UsersPage>(); });

        public RelayCommand RedirectToWordsCommand =>
            new RelayCommand((object obj) => { _redirect.To<WordsPage>(); });
    }
}