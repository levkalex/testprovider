﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Common.Entities;
using TestProviderUI.Annotations;
using CoreTestProvider.Common;
using CoreTestProvider.Queries;
using TestProviderUI.Utils;
using TestProviderUI.View;
using TestProviderUI.Core;

namespace TestProviderUI.VIewModels
{
	public class PassedTestsViewModel : BaseViewModel, IViewModel<PassedTestsPage>
	{
		private IDispatcher _dispatcher;
		private IRedirect _redirect;
		private IUserContext _userContext;

		public PassedTestsViewModel(IDispatcher dispatcher, IRedirect redirect, IUserContext userContext)
		{
			_dispatcher = dispatcher;
			_redirect = redirect;
			_userContext = userContext;
		}
		
		public ObservableCollection<PassedTest> PassedTests { get; set; }
		
		public ICommand FormLoadedCommand => new RelayAsyncCommand(FormLoaded);
		public ICommand ShowDetailCommand => new RelayAsyncCommand(ShowDetail);
		public ICommand BackToMainMenuCommand => new RelayCommand(BackToMainMenu);
		
		private async Task FormLoaded(object o)
		{
			_redirect.LockUI();
			var passedTests = await _dispatcher.Dispatch<IEnumerable<PassedTest>>(new PassedTestsFilterQuery() { Count = 1000, UserId = _userContext.Id });
			PassedTests = new ObservableCollection<PassedTest>(passedTests);
			
			OnPropertyChanged(nameof(PassedTests));
			_redirect.UnlockUI();
		}
		
		private async Task ShowDetail(object o)
		{
			_redirect.To<DetailPassedTestPage>("Detail test", new {id = (int) o});
		}
		
		private void BackToMainMenu(object index)
		{
			_redirect.To<MainPage>("Main menu");
		}
	}
}