﻿using AutoMapper;
using Common.Entities;
using Common.Models;
using CoreTestProvider.Commands;
using CoreTestProvider.Common;
using CoreTestProvider.Queries;
using Microsoft.Win32;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TestProviderUI.Core;
using TestProviderUI.Models;
using TestProviderUI.Utils;
using TestProviderUI.View;

namespace TestProviderUI.VIewModels
{
	public class UpdateQuestionViewModel : BaseViewModel, IViewModel<UpdateQuestionPage>
    {
        public BaseQuestionViewModel<BaseAnswerViewModel> Question { get; set; }
        public string ImagePath { get; set; }
        public string TextEditImageButton => (Question != null && Question.Image != null && Question.Image.Length > 0) ? "Edit image" : "Add image";
        public Visibility ImageDelButtonVisibility => (Question != null && Question.Image != null && Question.Image.Length > 0) ? Visibility.Visible : Visibility.Hidden;
        
        private IDispatcher _dispatcher;
        private IRedirect _redirect;
        private IMapper _mapper;
        private int _id;
        public UpdateQuestionViewModel(IDispatcher dispatcher, IMapper mapper, IRedirect redirect, int id)
        {
            _mapper = mapper;
            _dispatcher = dispatcher;
            _redirect = redirect;
            _id = id;
        }
        public ICommand SetImageCommand => new RelayCommand(SetImage);
        public ICommand DelImageCommand => new RelayCommand(DelImage);
        public ICommand FormLoadedCommand => new RelayAsyncCommand(FormLoaded);
        public ICommand BackToMainMenuCommand => new RelayCommand(BackToMainMenu);
        public ICommand SaveCommand => new RelayCommand(Save);

		

        private async Task FormLoaded(object o)
        {
            _redirect.LockUI();
            var question = await _dispatcher.Dispatch<Question>(new QuestionByIdFilterQuery { Id = _id });

            //var testQuestions = await _dispatcher.Dispatch<IEnumerable<PassedTestQuestionModel>>(new FetchFullPassedTestByIdFilterQuery() { PassedTestId = _passedTestId });
            Question = _mapper.Map<BaseQuestionViewModel<BaseAnswerViewModel>>(question);
            OnPropertyChanged(nameof(Question));
            OnPropertyChanged(nameof(ImagePath));
            OnPropertyChanged(nameof(TextEditImageButton));
            OnPropertyChanged(nameof(ImageDelButtonVisibility));

            _redirect.UnlockUI();
        }
        private void BackToMainMenu(object index)
        {
            _redirect.To<QuestionsPage>("All questions");
        }
        private void SetImage(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.FileName = string.Empty;
            openFileDialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
            "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
            "Portable Network Graphic (*.png)|*.png";
            if (openFileDialog.ShowDialog() == true)
            {
                ImagePath = openFileDialog.FileName;
            }
            if (!string.IsNullOrEmpty(ImagePath))
            {
                var img = Image.FromFile(ImagePath);
                using (MemoryStream ms = new MemoryStream())
                {
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    Question.Image = ms.ToArray();
                }
            }
            OnPropertyChanged(nameof(Question));
            OnPropertyChanged(nameof(ImagePath));
            OnPropertyChanged(nameof(TextEditImageButton));
            OnPropertyChanged(nameof(ImageDelButtonVisibility));
        }
        private void DelImage(object parameter)
        {
            Question.Image = default;
            ImagePath = string.Empty;
            OnPropertyChanged(nameof(Question));
            OnPropertyChanged(nameof(ImagePath));
            OnPropertyChanged(nameof(TextEditImageButton));
            OnPropertyChanged(nameof(ImageDelButtonVisibility));
        }
        private async void Save(object index)
        {
            //var question = _mapper.Map<Question>(Question);
           var question = new Question
            {
                Id = Question.Id,
                ShortName = Question.ShortName,
                Text = Question.Text,
                Description = Question.Description,
                Image = Question.Image,
                Answers = _mapper.Map<IEnumerable<Answer>>(Question.Answers).ToList(),
                IsEnabled = Question.IsEnabled
            };
            _redirect.LockUI();
            await _dispatcher.Dispatch<int>(new UpdateQuestionCommand { Question = question });
            _redirect.UnlockUI();
            MessageBox.Show("Saved!");
            _redirect.To<QuestionsPage>();
        }
    }
}
