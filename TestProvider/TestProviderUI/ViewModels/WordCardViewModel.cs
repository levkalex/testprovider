﻿using Common.Entities;
using Database.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestProviderUI.Attributes;
using TestProviderUI.Core;
using TestProviderUI.Utils;
using TestProviderUI.View;

namespace TestProviderUI.ViewModels
{
    [PageName("Word card")]
    public class WordCardViewModel : BaseViewModel, IViewModel<WordCardPage>
    {
        private IUnitOfWork _unitOfWork;
        private List<Word> _words;
        private IRedirect _redirect;

        public Word CurrentWord { get; set; }

        public WordCardViewModel(IUnitOfWork unitOfWork, IRedirect redirect)
        {
            _unitOfWork = unitOfWork;
            _redirect = redirect;
        }

        private async Task FormLoaded(object o)
        {
            _redirect.LockUI();

            //TODO: Replace to query
            var words = await _unitOfWork.WordRepository.FetchAllAsync();
            _words = words.ToList();

            CurrentWord = _words[_words.Count - 1];
            NextWord(null);

            _redirect.UnlockUI();
        }

        public ICommand FormLoadedCommand => new RelayAsyncCommand(FormLoaded);
        public ICommand NextWordCommand => new RelayCommand(NextWord);

        public void NextWord(object o)
        {
            var index = _words.IndexOf(CurrentWord);
            CurrentWord = _words[index == _words.Count - 1 ? 0 : index + 1];
            OnPropertyChanged(nameof(CurrentWord));
        }

    }
}
