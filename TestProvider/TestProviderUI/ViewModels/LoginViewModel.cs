﻿using Common.Exceptions;
using CoreTestProvider.Common;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using TestProviderUI.Core;
using TestProviderUI.Utils;
using TestProviderUI.View;

namespace TestProviderUI.ViewModels
{
    public class LoginViewModel : BaseViewModel, IViewModel<LoginPage>
    {
        public string Login { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsSavePassword { get; set; } = true;

        private IRedirect _redirect;
        private IPasswordProvider _passwordProvider;
        private IUserContext _userContext;

        public LoginViewModel(IRedirect redirect, IPasswordProvider passwordProvider, IUserContext userContext)
        {
            _redirect = redirect;
            _passwordProvider = passwordProvider;
            _userContext = userContext;
        }

        public ICommand FormLoadedCommand => new RelayAsyncCommand(FormLoaded);
        public ICommand LoginCommand => new RelayAsyncCommand(LoginAccount);

        private async Task FormLoaded(object o)
        {
            _redirect.LockUI();

            var result = await _userContext.FetchCredentialAsync();
            if (result != null)
            {
                Login = result.Username;
                OnPropertyChanged(nameof(Login));
                try
                {
                    await _userContext.SignInAsync(result.Username, result.SecurePassword, false);
                    _redirect.To<MainPage>("Main page");
                }
                catch (AuthenticationException e) { }
            }

            _redirect.UnlockUI();
        }

        private async Task LoginAccount(object o)
        {
            _redirect.LockUI();

            var password = _passwordProvider.GetPassword;

            try
            {
                await _userContext.SignInAsync(Login, password, IsSavePassword);
                _redirect.To<MainPage>("Main page");
            }
            catch (AuthenticationException e)
            {
                ErrorMessage = e.Message;
                OnPropertyChanged(nameof(ErrorMessage));
            }
            finally
            {
                _redirect.UnlockUI();
            }
        }
    }
}
