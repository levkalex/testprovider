﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using AutoMapper;
using CoreTestProvider.Common;
using CoreTestProvider.Queries;
using TestProviderUI.Annotations;
using TestProviderUI.Utils;
using Common.Entities;
using Common.Models;
using CoreTestProvider.Commands;
using TestProviderUI.View;
using Database.Common;
using TestProviderUI.Core;
using TestProviderUI.Models;
using Microsoft.Extensions.Configuration;
using Common.Constants;
using System;
using System.Windows.Controls;

namespace TestProviderUI.VIewModels
{
    public class TestViewModel : BaseViewModel, IViewModel<TestPage>
    {
        private TestQuestionViewModel _selectedQuestion;
        private IDispatcher _dispatcher;
        private IMapper _mapper;
        private IRedirect _redirect;
        private IConfiguration _configuration;
        private int _passedTestId;
        public TestViewModel(IUnitOfWork unitOfWork, IDispatcher dispatcher, IMapper mapper, IRedirect redirect, IConfiguration configuration)
        {
            _dispatcher = dispatcher;
            _mapper = mapper;
            _configuration = configuration;
            _redirect = redirect;
        }
        
        public bool IsLastQuestion => Questions != null && _selectedQuestion.Id == Questions.Last().Id;
        public bool IsFinishTest => IsLastQuestion && Questions.SkipLast(1).All(x => x.Answers.Any(x => x.IsSelected));
        public ObservableCollection<TestQuestionViewModel> Questions { get; set; }
        public int CountCheckAnswer => Questions != null ? Questions.Where(x => x.TopButton.Color != null && x.TopButton.Color.Color == Colors.LightGray).Count() : 0;
        public int CountQuestions => Questions != null ? Questions.Count : 0;
        
        public TestQuestionViewModel SelectedQuestion
        {
            get => _selectedQuestion;
            set
            {
                if (_selectedQuestion != null)
                    _selectedQuestion.TopButton.Color = _selectedQuestion.Answers.Any(x => x.IsSelected)
                        ? new SolidColorBrush(Colors.LightGray)
                        : new SolidColorBrush(Color.FromArgb(255, (238), (243), (247)));
                _selectedQuestion = value;
                value.TopButton.Color = new SolidColorBrush(Colors.OrangeRed);

                OnPropertyChanged(nameof(SelectedQuestion));
                OnPropertyChanged(nameof(IsFinishTest));
                OnPropertyChanged(nameof(CountCheckAnswer));
            }
        }
        
        public ICommand ChangeSelectedCommand => new RelayCommand(ChangeSelectedQuestion);
        public ICommand NextQuestionCommand => new RelayCommand(NextQuestion);
        public ICommand FinishTestCommand => new RelayAsyncCommand(FinishTest);
        public ICommand FormLoadedCommand => new RelayAsyncCommand(FormLoaded);
        public ICommand BackToMainMenuCommand => new RelayCommand(BackToMainMenu);

        private async Task FormLoaded(object o)
        {
            _redirect.LockUI();
            var countQuestion = Convert.ToInt32(_configuration[AppSettingKeys.COUNT_QUESTION]);
            var questions = await _dispatcher.Dispatch<IEnumerable<Question>>(new FetchQuestionsWithAnswersFilterQuery { Count = countQuestion, IsEnabled = true }) ;
            var mappedQuestions = _mapper.Map<IEnumerable<TestQuestionViewModel>>(questions);
            Questions = new ObservableCollection<TestQuestionViewModel>(mappedQuestions);
            Questions.SetIndex();
            OnPropertyChanged(nameof(Questions));
            OnPropertyChanged(nameof(CountQuestions));
            ChangeSelectedCommand.Execute(1);
            await SaveStartingTest();
            _redirect.UnlockUI();
        }

        public async Task SaveStartingTest()
        {
            _passedTestId = await _dispatcher.Dispatch<int>(new StartTestCommand() {QuestionIds = Questions.Select(x => x.Id).ToArray()});
        }
        
        /// <param name="index">Start with 1</param>
        private void ChangeSelectedQuestion(object index)
        {
            SelectedQuestion = Questions[(int) index - 1];
        }

        private void NextQuestion(object index)
        {
            if (IsLastQuestion == false)
                ChangeSelectedQuestion(Questions.IndexOf(SelectedQuestion) + 2);
            else
            {
                var firstNonAnswerQuestion = Questions.First(x => x.Answers.All(y => !y.IsSelected));
                ChangeSelectedQuestion(Questions.IndexOf(firstNonAnswerQuestion) + 1);
            }
        }

        private async Task FinishTest(object t)
        {
            var passedTestAnswer = _mapper.Map<IEnumerable<PassedTestAnswerModel>>(Questions.SelectMany(x => x.Answers));
            await _dispatcher.Dispatch<int>(new FinishTestCommand()
            {
                PassedTestId = _passedTestId, PassedTestAnswers = passedTestAnswer.ToArray()
            });

            _redirect.To<DetailPassedTestPage>("Detail test", new {id = _passedTestId});
        }
        
        private void BackToMainMenu(object index)
        {
            _redirect.To<MainPage>("Main menu");
        }
    }
}