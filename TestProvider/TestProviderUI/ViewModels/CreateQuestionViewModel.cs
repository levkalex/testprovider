﻿using AutoMapper;
using CoreTestProvider.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using TestProviderUI.Utils;
using TestProviderUI.View;
using Common.Models;
using System.Windows;
using CoreTestProvider.Commands;
using Microsoft.Win32;
using System.IO;
using TestProviderUI.Core;
using System.Drawing;
using TestProviderUI.Models;
using Common.Entities;

namespace TestProviderUI.VIewModels
{
	public class CreateQuestionViewModel : BaseViewModel, IViewModel<CreateQuestionPage>
    {
        public BaseQuestionViewModel<BaseAnswerViewModel> Question { get; set; }
		public string ImagePath { get; set; }
		public string TextEditImageButton => string.IsNullOrEmpty(ImagePath) ? "Add image" : "Edit image";
        public Visibility ImageDelButtonVisibility => string.IsNullOrEmpty(ImagePath) ? Visibility.Hidden : Visibility.Visible;

        public ICommand SetImageCommand => new RelayCommand(SetImage);
        public ICommand DelImageCommand => new RelayCommand(DelImage);
        public ICommand AddCommand => new RelayCommand(Add);
        public ICommand DelCommand => new RelayCommand(Del);
        public ICommand SaveCommand => new RelayCommand(Save);
        public ICommand BackToMainMenuCommand => new RelayCommand(BackToMainMenu);
        public IDispatcher Dispatcher { get; private set; }
        private IRedirect _redirect;
        private IMapper _mapper;

        public CreateQuestionViewModel(IDispatcher dispatcher, IMapper mapper, IRedirect redirect)
        {
            this.Dispatcher = dispatcher;
            _redirect = redirect;
            _mapper = mapper;
            Question = new BaseQuestionViewModel<BaseAnswerViewModel>();
            Question.Answers = new List<BaseAnswerViewModel>();
            Question.Answers.Add(new BaseAnswerViewModel());
            Question.Answers.Add(new BaseAnswerViewModel());
            Question.Answers.Add(new BaseAnswerViewModel());
        }

        private void SetImage(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.FileName = string.Empty;
            openFileDialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
            "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
            "Portable Network Graphic (*.png)|*.png";
            if (openFileDialog.ShowDialog() == true)
            {
                ImagePath = openFileDialog.FileName;
            }
            OnPropertyChanged(nameof(ImagePath));
            OnPropertyChanged(nameof(TextEditImageButton));
            OnPropertyChanged(nameof(ImageDelButtonVisibility));
        }
        private void DelImage(object parameter)
        {
            ImagePath = string.Empty;
            OnPropertyChanged(nameof(ImagePath));
            OnPropertyChanged(nameof(TextEditImageButton));
            OnPropertyChanged(nameof(ImageDelButtonVisibility));
        }
        private void Add(object parameter)
        {
            if (Question.Answers.Count < 8)
                Question.Answers.Add(new BaseAnswerViewModel());
        }
        private void Del(object parameter)
        {
            if (Question.Answers.Count > 3)
                Question.Answers.RemoveAt(Question.Answers.Count - 1);
        }
        private void Save(object parameter)
        {
            _redirect.LockUI();
            byte[] image = default;
            if (!string.IsNullOrEmpty(ImagePath))
            {
                var img = Image.FromFile(ImagePath);
                using (MemoryStream ms = new MemoryStream())
                {
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    image = ms.ToArray();
                }
            }
            var command = new CreateQuestionCommand
            {
                Id = Question.Id,
                ShortName = Question.ShortName,
                Text = Question.Text,
                Description = Question.Description,
                Image = image,
                Answers = _mapper.Map<IEnumerable<Answer>>(Question.Answers).ToList(),
                IsEnabled = true
            };
            Dispatcher.Dispatch<int>(command);
            _redirect.UnlockUI();
            MessageBox.Show("Created");
            _redirect.To<QuestionsPage>("Questions");
        }

        private void BackToMainMenu(object index)
        {
            _redirect.To<MainPage>("Main menu");
        }
    }
}
