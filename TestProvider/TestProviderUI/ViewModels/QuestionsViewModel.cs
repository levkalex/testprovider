﻿using Common.Entities;
using CoreTestProvider.Common;
using CoreTestProvider.Queries;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using TestProviderUI.Attributes;
using TestProviderUI.Core;
using TestProviderUI.Utils;
using TestProviderUI.View;

namespace TestProviderUI.VIewModels
{
	[PageName("Questions list")]
	class QuestionsViewModel : BaseViewModel, IViewModel<QuestionsPage>
	{
		private IDispatcher _dispatcher;
		private IRedirect _redirect;

		public QuestionsViewModel(IDispatcher dispatcher, IRedirect redirect)
		{
			_dispatcher = dispatcher;
			_redirect = redirect;
			IsEnabled = true;
			OnPropertyChanged(nameof(IsEnabled));
		}
		public ICommand FormLoadedCommand => new RelayAsyncCommand(FormLoaded);
		public ICommand BackToMainMenuCommand => new RelayCommand(BackToMainMenu);
		public ICommand ShowCommand => new RelayCommand(Show);
		public ICommand EditCommand => new RelayCommand(Edit);
		public ICommand EnableCommand => new RelayCommand(EnableChanged);
		public ObservableCollection<Question> Questions { get; set; }
		public bool IsEnabled { get; set; }
		public int Count => Questions != null ? Questions.Count : 0;

		private async Task FormLoaded(object o)
		{
			_redirect.LockUI();
			var questions = await _dispatcher.Dispatch<IEnumerable<Question>>(new FetchQuestionsFilterQuery { Count = 1000, IsEnabled = IsEnabled });
			Questions = new ObservableCollection<Question>(questions);
			OnPropertyChanged(nameof(Questions));
			OnPropertyChanged(nameof(Count));
			_redirect.UnlockUI();
		}
		private async void EnableChanged(object index) 
		{
			_redirect.LockUI();
			var questions = await _dispatcher.Dispatch<IEnumerable<Question>>(new FetchQuestionsFilterQuery { Count = 1000, IsEnabled = IsEnabled });
			Questions = new ObservableCollection<Question>(questions);
			OnPropertyChanged(nameof(Questions));
			OnPropertyChanged(nameof(Count));
			_redirect.UnlockUI();
		}
		private void BackToMainMenu(object index)
		{
			_redirect.To<MainPage>("Main menu");
		}
		private void Show(object index)
		{
			_redirect.To<DetailQuestionPage>("View question", new { id = index });
		}
		private void Edit(object index) 
		{
			_redirect.To<UpdateQuestionPage>("Edit question", new { id = index });
		}
	}
}
