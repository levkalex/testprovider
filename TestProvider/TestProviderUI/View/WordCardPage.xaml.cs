﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TestProviderUI.Annotations;
using TestProviderUI.Core;
using TestProviderUI.ViewModels;

namespace TestProviderUI.View
{
    /// <summary>
    /// Interaction logic for WordCardPage.xaml
    /// </summary>
    public partial class WordCardPage : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        IRedirect _redirect;
        public WordCardPage(IRedirect redirect)
        {
            InitializeComponent();

            _redirect = redirect;

            Loaded += MyWindow_Loaded;
            this.SizeChanged += Change;
        }

        private async void Change(object sender, RoutedEventArgs e)
        {
            OnPropertyChanged(nameof(MaxSquare));
            OnPropertyChanged(nameof(HalpSizeSquare));
        }

        private async void MyWindow_Loaded(object sender, RoutedEventArgs e)
        {
            StartAnimation();
        }

        public double HalpSizeSquare => MaxSquare / 1.35;
        public double MaxSquare
        {
            get
            {
                if (_this.ActualWidth > 850 && _this.ActualHeight > 850)
                    return 550;

                if (this.ActualWidth - 300 < 80 || _this.ActualHeight - 300 < 80)
                    return 80;

                if (_this.ActualWidth > _this.ActualHeight)
                {
                    return _this.ActualHeight - 300;
                }
                else
                {
                    return this.ActualWidth - 300;
                }
            }
        }

        public async void StartAnimation()
        {
            var sb = new Storyboard();

            var ta = new ThicknessAnimation();
            ta.SetValue(Storyboard.TargetNameProperty, "Block");
            ta.BeginTime = new TimeSpan(0);

            //SystemParameters.PrimaryScreenWidth;
            ta.From = new Thickness(this.ActualWidth + 400, 0, 0, 0);
            ta.To = new Thickness(0, 0, 0, 0);
            ta.Duration = new Duration(TimeSpan.FromSeconds(1));
            Storyboard.SetTargetProperty(ta, new PropertyPath(MarginProperty));

            var ta2 = new DoubleAnimation();
            ta2.SetValue(Storyboard.TargetNameProperty, "Block");
            ta2.BeginTime = TimeSpan.FromMilliseconds(250);
            ta2.From = Block.ActualHeight;
            ta2.To = Convert.ToInt32(MaxSquare);
            ta2.Duration = new Duration(TimeSpan.FromMilliseconds(750));
            Storyboard.SetTargetProperty(ta2, new PropertyPath(HeightProperty));

            var ta3 = new DoubleAnimation();
            ta3.SetValue(Storyboard.TargetNameProperty, "Block");
            ta3.BeginTime = TimeSpan.FromMilliseconds(250);
            ta3.From = Block.ActualWidth;
            ta3.To = Convert.ToInt32(MaxSquare);
            ta3.Duration = new Duration(TimeSpan.FromMilliseconds(750));
            Storyboard.SetTargetProperty(ta3, new PropertyPath(WidthProperty));

            sb.Children.Add(ta);
            sb.Children.Add(ta2);
            sb.Children.Add(ta3);
            sb.Completed += (object? sender, EventArgs e) => {
                Block.BeginAnimation(Grid.WidthProperty, null);
                Block.BeginAnimation(Grid.HeightProperty, null);
                isRun = false;
            };
            sb.Begin(this);
        }

        public bool isRun = false;
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (isRun)
                return;
            isRun = true;

            var sb2 = new Storyboard();

            var ta4 = new ThicknessAnimation();
            ta4.SetValue(Storyboard.TargetNameProperty, "Block");
            ta4.BeginTime = new TimeSpan(0);
            ta4.From = new Thickness(0, 0, 0, 0);
            ta4.To = new Thickness(0, 0, this.ActualWidth + 800, 0);
            ta4.Duration = new Duration(TimeSpan.FromSeconds(1));
            Storyboard.SetTargetProperty(ta4, new PropertyPath(MarginProperty));

            var ta5 = new DoubleAnimation();
            ta5.SetValue(Storyboard.TargetNameProperty, "Block");
            ta5.BeginTime = TimeSpan.FromMilliseconds(0);
            ta5.From = Block.ActualHeight;
            ta5.To = Convert.ToInt32(HalpSizeSquare);
            ta5.Duration = new Duration(TimeSpan.FromMilliseconds(750));
            Storyboard.SetTargetProperty(ta5, new PropertyPath(StackPanel.HeightProperty));

            var ta6 = new DoubleAnimation();
            ta6.SetValue(Storyboard.TargetNameProperty, "Block");
            ta6.BeginTime = TimeSpan.FromMilliseconds(0);
            ta6.From = Block.ActualWidth;
            ta6.To = Convert.ToInt32(HalpSizeSquare);
            ta6.Duration = new Duration(TimeSpan.FromMilliseconds(750));
            Storyboard.SetTargetProperty(ta6, new PropertyPath(StackPanel.WidthProperty));

            sb2.Children.Add(ta4);
            sb2.Children.Add(ta5);
            sb2.Children.Add(ta6);
            sb2.Completed += (object? sender, EventArgs e) => {
                var viewModel = (WordCardViewModel)this.DataContext;
                viewModel.NextWord(null);
                StartAnimation();
            };
            sb2.Begin(this);

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            _redirect.To<MainPage>();
        }
    }

}
