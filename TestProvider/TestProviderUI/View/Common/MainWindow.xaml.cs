﻿using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using TestProviderUI.View;

namespace TestProviderUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Start_OnClick(object sender, RoutedEventArgs e)
        {
        }

		private void Add_Click(object sender, RoutedEventArgs e)
		{
            //AddingQuestionWindow addingQuestion = new AddingQuestionWindow();
            //addingQuestion.ShowDialog();
        }
	}
}