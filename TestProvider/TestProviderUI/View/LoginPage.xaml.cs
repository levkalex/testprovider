﻿using System.Security;
using System.Windows.Controls;

namespace TestProviderUI.View
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : UserControl, IPasswordProvider
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        public SecureString GetPassword => PasswordBox.SecurePassword;
    }

    public interface IPasswordProvider
    {
        public SecureString GetPassword { get; }
    }
}
