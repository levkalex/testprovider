﻿using Autofac;
using Autofac.Core;
using Common.Utils.Extensions;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TestProviderUI.Attributes;
using TestProviderUI.View.Common;

namespace TestProviderUI.Core
{
    public interface IRedirect
    {
        void To<T>(string title = Redirect.Empty, object parameters = null) where T : UserControl;

        void LockUI();
        void UnlockUI();
    }

    public class Redirect : IRedirect
    {
        MainWindow _window;
        ILifetimeScope _scope;

        public const string Empty = "";

        public Redirect(ILifetimeScope scope)
        {
            _scope = scope;
            _window = new MainWindow();
            _window.Show();
        }

        ILifetimeScope scopePerPage;
        public void To<T>(string title = Empty, object parameters = null) where T : UserControl
        {
            //TODO: Refactor
            if (scopePerPage != null)
                scopePerPage.Dispose();

            scopePerPage = _scope.BeginLifetimeScope();

            var page = scopePerPage.Resolve<T>();
            page.HorizontalAlignment = HorizontalAlignment.Stretch;
            page.VerticalAlignment = VerticalAlignment.Stretch;

            var viewModelType = scopePerPage.ComponentRegistry.Registrations.Single(x => x.Services
                    .OfType<IServiceWithType>()
                    .Any(x => typeof(IViewModel<T>)
                        .IsAssignableFrom(x.ServiceType)))
                .Activator.LimitType;

            var viewModel = parameters == null ? scopePerPage.Resolve(viewModelType) :
                                                 scopePerPage.Resolve(viewModelType, GetNamedParameters(parameters));
            page.DataContext = viewModel;

            _window.Title = GetPageName(title, viewModel.GetType());
            _window.Content.Children.Clear();
            _lockPage = null;
            _window.Content.Children.Add(page);
        }

        private string GetPageName(string title, Type viewModel)
        {
            if (title != String.Empty)
                return title;
            var attributeValue = viewModel.GetAttributeValue((PageNameAttribute pn) => pn.Name);
            return attributeValue ?? String.Empty;
        }

        private NamedParameter[] GetNamedParameters(object parameters)
        {
            if (parameters != null)
            {
                var anonType = parameters.GetType();
                var properties = anonType.GetProperties();

                NamedParameter[] namedParameters = new NamedParameter[properties.Count()];
                for (int i = 0; i < properties.Count(); i++)
                {
                    object propValue = properties[i].GetValue(parameters, null);
                    namedParameters[i] = new NamedParameter(properties[i].Name, propValue);
                }
                return namedParameters;
            }
            return null;
        }

        private LockUIPage _lockPage;
        public void LockUI()
        {
            _lockPage = new LockUIPage();
            _window.Content.Children.Add(_lockPage);
        }

        public void UnlockUI()
        {
            if (_lockPage != null)
                _window.Content.Children.Remove(_lockPage);
        }
    }
}
