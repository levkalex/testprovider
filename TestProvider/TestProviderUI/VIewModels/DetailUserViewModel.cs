﻿using AutoMapper;
using Common.Entities;
using CoreTestProvider.Common;
using CoreTestProvider.Queries;
using System.Threading.Tasks;
using System.Windows.Input;
using TestProviderUI.Core;
using TestProviderUI.Utils;
using TestProviderUI.View;

namespace TestProviderUI.VIewModels
{
	class DetailUserViewModel : BaseViewModel, IViewModel<DetailUserPage>
	{
        public User User { get; set; }
        private IDispatcher _dispatcher;
        private IRedirect _redirect;
        private int _id;
        public DetailUserViewModel(IDispatcher dispatcher, IMapper mapper, IRedirect redirect, int id)
        {
            _dispatcher = dispatcher;
            _redirect = redirect;
            _id = id;
        }

        public ICommand FormLoadedCommand => new RelayAsyncCommand(FormLoaded);
        public ICommand BackToUsersCommand => new RelayCommand(BackToMainMenu);
        private async Task FormLoaded(object o)
        {
            _redirect.LockUI();
            User = await _dispatcher.Dispatch<User>(new UserByIdFilterQuery { Id = _id });
            OnPropertyChanged(nameof(User));
            _redirect.UnlockUI();
        }
        private void BackToMainMenu(object index)
        {
            _redirect.To<UsersPage>("All users");
        }
    }
}
