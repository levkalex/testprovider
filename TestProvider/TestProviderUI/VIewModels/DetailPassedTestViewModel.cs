﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using AutoMapper;
using Common.Entities;
using Common.Models;
using CoreTestProvider.Common;
using CoreTestProvider.Queries;
using TestProviderUI.Annotations;
using TestProviderUI.Core;
using TestProviderUI.Models;
using TestProviderUI.Utils;
using TestProviderUI.View;

namespace TestProviderUI.VIewModels
{
    public class DetailPassedTestViewModel: BaseViewModel, IViewModel<DetailPassedTestPage>
    {
        private TestQuestionViewModel  _selectedPassedQuestion;
        private IDispatcher _dispatcher;
        private IRedirect _redirect;
        private IMapper _mapper;
        private int _passedTestId;
        
        public DetailPassedTestViewModel(IDispatcher dispatcher, IMapper mapper, IRedirect redirect, int id)
        {
            _dispatcher = dispatcher;
            _passedTestId = id;
            _mapper = mapper;
            _redirect = redirect;
        }
        
        public TestQuestionViewModel SelectedQuestion
        {
            get => _selectedPassedQuestion;
            set
            {
               if (_selectedPassedQuestion != null)
                    _selectedPassedQuestion.TopButton.Border = 0;

                _selectedPassedQuestion = value;

                _selectedPassedQuestion.TopButton.Border = 1;
                _selectedPassedQuestion.Answers.ForEach(x =>
                {
                    if (x.IsRight && x.IsSelected)
                        x.BackgroundColor = new SolidColorBrush(Color.FromArgb(255, (161), (219), (132)));
                    else if (!x.IsRight && x.IsSelected)
                        x.BackgroundColor = new SolidColorBrush(Colors.Red);
                    else if (x.IsRight && !x.IsSelected)
                        x.BackgroundColor = new SolidColorBrush(Color.FromArgb(255, (0), (122), (204)));
                });

                OnPropertyChanged(nameof(SelectedQuestion));
            }
        }

        public ObservableCollection<TestQuestionViewModel> Questions { get; set; }
        public int PercentResult { get; set; }
        public ICommand ChangeSelectedCommand => new RelayCommand(ChangeSelectedQuestion);
        public ICommand FormLoadedCommand => new RelayAsyncCommand(FormLoaded);
        public ICommand NextQuestionCommand => new RelayCommand(NextQuestion);
        public ICommand PrevQuestionCommand => new RelayCommand(PrevQuestion);
        public ICommand BackToMainMenuCommand => new RelayCommand(BackToMainMenu);
        
        private async Task FormLoaded(object o)
        {
            _redirect.LockUI();
            
            var passedTest = await _dispatcher.Dispatch<PassedTest>(new PassedTestByIdFilterQuery {Id = _passedTestId});
            PercentResult = passedTest.PercentResult;
            OnPropertyChanged(nameof(PercentResult));
            
            var testQuestions =  await _dispatcher.Dispatch<IEnumerable<PassedTestQuestionModel>>(new FetchFullPassedTestByIdFilterQuery() {PassedTestId = _passedTestId});
            var testQuestionsMapped = _mapper.Map<IEnumerable<TestQuestionViewModel>>(testQuestions);

            SetColorTopButtons(testQuestionsMapped);

            Questions = new ObservableCollection<TestQuestionViewModel>(testQuestionsMapped);
            OnPropertyChanged(nameof(Questions));
            ChangeSelectedCommand.Execute(1);
            _redirect.UnlockUI();
        }

        private void SetColorTopButtons(IEnumerable<TestQuestionViewModel> passedTestQuestions)
        {
            var index = 1;
            foreach (var item in passedTestQuestions)
            {
                var rightCount = item.Answers.Count(x => x.IsRight && x.IsSelected);
                var wrongCount = item.Answers.Count(x => (!x.IsRight && x.IsSelected) || (x.IsRight && !x.IsSelected));

                item.TopButton = new TopButtonViewModel() { Index = index, Border = 0 };

                if (wrongCount == 0)
                    item.TopButton.Color = new SolidColorBrush(Color.FromArgb(255, (161), (219), (132)));
                else if (rightCount == 0)
                    item.TopButton.Color = new SolidColorBrush(Colors.Red);
                else
                    item.TopButton.Color = new SolidColorBrush(Colors.Yellow);

                index++;
            }
        }
        
        /// <param name="index">Start with 1</param>
        private void ChangeSelectedQuestion(object index)
        {
            SelectedQuestion = Questions[(int) index - 1];
        }

        private void NextQuestion(object o)
        {
            var index = Questions.IndexOf(SelectedQuestion);
            if (index < Questions.Count() - 1)
                ChangeSelectedQuestion(index + 2);
            else
                ChangeSelectedQuestion(1);
        }

        private void PrevQuestion(object o)
        {
            var index = Questions.IndexOf(SelectedQuestion);
            if (index > 0)
                ChangeSelectedQuestion(index);
            else
                ChangeSelectedQuestion(Questions.Count());
        }

        private void BackToMainMenu(object index)
        {
            _redirect.To<PassedTestsPage>("History window");
        }
    }
}