﻿using Common.Entities;
using CoreTestProvider.Common;
using CoreTestProvider.Queries;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using TestProviderUI.Attributes;
using TestProviderUI.Core;
using TestProviderUI.Utils;
using TestProviderUI.View;

namespace TestProviderUI.VIewModels
{
	class UsersViewModel: BaseViewModel, IViewModel<UsersPage>
	{
		private IDispatcher _dispatcher;
		private IRedirect _redirect;
		public UsersViewModel(IDispatcher dispatcher, IRedirect redirect)
		{
			_dispatcher = dispatcher;
			_redirect = redirect;
		}
		public ICommand FormLoadedCommand => new RelayAsyncCommand(FormLoaded);
		public ICommand BackToMainMenuCommand => new RelayCommand(BackToMainMenu);
		public ICommand ShowCommand => new RelayCommand(Show);
		public ObservableCollection<User> Users { get; set; }
		public int Count => Users != null ? Users.Count : 0;
		private async Task FormLoaded(object o)
		{
			_redirect.LockUI();
			var users = await _dispatcher.Dispatch<IEnumerable<User>>(new FetchUsersFilterQuery { Count = 1000});
			Users = new ObservableCollection<User>(users);
			OnPropertyChanged(nameof(Users));
			OnPropertyChanged(nameof(Count));
			_redirect.UnlockUI();
		}
		private void BackToMainMenu(object index)
		{
			_redirect.To<MainPage>("Main menu");
		}
		private void Show(object index)
		{
			_redirect.To<DetailUserPage>("View user", new { id = index });
		}
	}
}
