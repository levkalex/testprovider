﻿using System;

namespace TestProviderUI.Attributes
{
    public class PageNameAttribute : Attribute
    {
        public string Name { get; }
        public PageNameAttribute(string name)
        {
            Name = name;
        }
    }
}
