﻿using System.Collections.Generic;
using TestProviderUI.Core;

namespace TestProviderUI.Models
{
    public class BaseQuestionViewModel<TAnswer> : BaseViewModel 
                                        where TAnswer : BaseAnswerViewModel
    {
        public int Id { get; set; }
        public string ShortName { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
       
        private byte[] _image;
        public byte[] Image
        {
            get => _image;
            set
            {
                _image = value;
                OnPropertyChanged(nameof(Image));
            }
        }

        public bool IsEnabled { get; set; }

        public List<TAnswer> Answers { get; set; }
    }

    public class BaseAnswerViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsRight { get; set; }
        public int QuestionId { get; set; }
    }
}
