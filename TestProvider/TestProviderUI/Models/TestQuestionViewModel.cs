﻿using System.Windows.Media;

namespace TestProviderUI.Models
{
    public class TestQuestionViewModel : BaseQuestionViewModel<TestAnswerViewModel>
    {
        private TopButtonViewModel _topButton;
        public TopButtonViewModel TopButton
        {
            get => _topButton;
            set
            {
                _topButton = value;
                OnPropertyChanged(nameof(TopButton));
            }
        }
    }

    public class TestAnswerViewModel : BaseAnswerViewModel
    {
        public bool IsSelected { get; set; }
        public SolidColorBrush BackgroundColor { get; set; }
    }
}
