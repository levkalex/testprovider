﻿using System.Windows.Media;
using TestProviderUI.Core;

namespace TestProviderUI.Models
{
    public class TopButtonViewModel : BaseViewModel
    {
        private SolidColorBrush _color;
        private int _index;
        private int _border;

        public int Index
        {
            get => _index;
            set
            {
                _index = value;
                OnPropertyChanged(nameof(Index));
            }
        }

        public int Border
        {
            get => _border;
            set
            {
                _border = value;
                OnPropertyChanged(nameof(Border));
            }
        }


        public SolidColorBrush Color
        {
            get => _color;
            set
            {
                _color = value;
                OnPropertyChanged(nameof(Color));
            }
        }
    }
}
