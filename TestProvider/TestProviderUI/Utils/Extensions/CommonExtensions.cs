﻿using System.Collections.Generic;
using TestProviderUI.Models;

namespace TestProviderUI.Utils
{
	public static class CommonExtensions
	{
		public static void SetIndex(this IEnumerable<TestQuestionViewModel> questions)
		{
			var count = 1;
			foreach (var item in questions)
			{
				item.TopButton = new TopButtonViewModel();
				item.TopButton.Index = count;
				count++;
			}
		}
	}
}
