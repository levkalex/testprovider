using Common.Models;
using CoreTestProvider.Utils;
using NUnit.Framework;
using System;

namespace CoreTestProvider.Tests
{
    public class CalculateTessResultTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void OneQuestionOneAnswerOfOneRightTest()
        {
            var answers = new PassedTestAnswerModel[3]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = false, IsSelected = false }
            };

            var result= CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.EqualTo(100));
        }

        [Test]
        public void OneQuestionOneRightAnswerOfTwoRightTest()
        {
            var answers = new PassedTestAnswerModel[3]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = true, IsSelected = false }
            };

            var result = CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.EqualTo(50));
        }

        [Test]
        public void OneQuestionThreeWrongAnswersOfOneRightTest()
        {
            var answers = new PassedTestAnswerModel[3]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = true },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = true, IsSelected = false },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = false, IsSelected = true }
            };

            var result = CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.Zero);
        }

        [Test]
        public void OneQuestionTwoRightOfThreeRightTest()
        {
            var answers = new PassedTestAnswerModel[3]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = true, IsSelected = false },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = true, IsSelected = true }
            };

            var result = CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.EqualTo(66));
        }

        [Test]
        public void OneQuestionOneRightOneWrongAnswersOfTwoRightTest()
        {
            var answers = new PassedTestAnswerModel[3]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = true },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = true, IsSelected = false }
            };

            var result = CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.Zero);
        }

        [Test]
        public void TwoQuestionsAllRightTest()
        {
            var answers = new PassedTestAnswerModel[6]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 4, QuestionId = 2, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 5, QuestionId = 2, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 6, QuestionId = 2, IsRight = true, IsSelected = true }
            };

            var result = CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.EqualTo(100));
        }

        [Test]
        public void TwoQuestionsOneRightTest()
        {
            var answers = new PassedTestAnswerModel[6]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 4, QuestionId = 2, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 5, QuestionId = 2, IsRight = true, IsSelected = false },
                new PassedTestAnswerModel { Id = 6, QuestionId = 2, IsRight = true, IsSelected = false }
            };

            var result = CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.EqualTo(50));
        }

        [Test]
        public void TwoQuestionsOneRightOneWrongTest()
        {
            var answers = new PassedTestAnswerModel[6]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 4, QuestionId = 2, IsRight = false, IsSelected = true },
                new PassedTestAnswerModel { Id = 5, QuestionId = 2, IsRight = false, IsSelected = true },
                new PassedTestAnswerModel { Id = 6, QuestionId = 2, IsRight = true, IsSelected = false }
            };

            var result = CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.EqualTo(50));
        }

        [Test]
        public void TwoQuestionsFirstRightSecondHalfRightTest()
        {
            var answers = new PassedTestAnswerModel[6]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 4, QuestionId = 2, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 5, QuestionId = 2, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 6, QuestionId = 2, IsRight = true, IsSelected = false }
            };

            var result = CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.EqualTo(75));
        }

        [Test]
        public void TwoQuestionsFirstRightSecondWrongRightTest()
        {
            var answers = new PassedTestAnswerModel[6]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 4, QuestionId = 2, IsRight = false, IsSelected = true },
                new PassedTestAnswerModel { Id = 5, QuestionId = 2, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 6, QuestionId = 2, IsRight = true, IsSelected = false }
            };

            var result = CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.EqualTo(50));
        }

        [Test]
        public void ThreeQuestionsOneRight()
        {
            var answers = new PassedTestAnswerModel[9]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 4, QuestionId = 2, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 5, QuestionId = 2, IsRight = true, IsSelected = false },
                new PassedTestAnswerModel { Id = 6, QuestionId = 2, IsRight = true, IsSelected = false },
                new PassedTestAnswerModel { Id = 7, QuestionId = 3, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 8, QuestionId = 3, IsRight = true, IsSelected = false },
                new PassedTestAnswerModel { Id = 9, QuestionId = 3, IsRight = true, IsSelected = false }
            };

            var result = CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.EqualTo(33));
        }

        [Test]
        public void ThreeQuestionsFirstRightSecondHalfRight()
        {
            var answers = new PassedTestAnswerModel[9]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 2, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 4, QuestionId = 2, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 5, QuestionId = 2, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 6, QuestionId = 2, IsRight = true, IsSelected = false },
                new PassedTestAnswerModel { Id = 7, QuestionId = 3, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 8, QuestionId = 3, IsRight = true, IsSelected = false },
                new PassedTestAnswerModel { Id = 9, QuestionId = 3, IsRight = true, IsSelected = false }
            };

            var result = CalculateTestResult.Calculate(answers);

            Assert.That(result, Is.EqualTo(50));
        }



        [Test]
        public void NotScecifyQuestionId()
        {
            var answers = new PassedTestAnswerModel[3]
            {
                new PassedTestAnswerModel { Id = 1, QuestionId = 1, IsRight = false, IsSelected = false },
                new PassedTestAnswerModel { Id = 2, IsRight = true, IsSelected = true },
                new PassedTestAnswerModel { Id = 3, QuestionId = 1, IsRight = false, IsSelected = false }
            };

            var ex = Assert.Throws<ArgumentException>(() => CalculateTestResult.Calculate(answers));
            Assert.That(ex.Message == "Not all answers have QuestionId");
        }
    }
}