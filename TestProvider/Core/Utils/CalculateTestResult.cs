﻿using Common.Models;
using System;
using System.Linq;

namespace CoreTestProvider.Utils
{
    public static class CalculateTestResult
    {
        public static int Calculate(PassedTestAnswerModel[] passedTestAnswers)
        {
            if (!passedTestAnswers.All(x => x.QuestionId > 0))
                throw new ArgumentException("Not all answers have QuestionId");

            var answerByGroup = passedTestAnswers.GroupBy(x => x.QuestionId);

            var questionsCount = answerByGroup.Count();
            var percentForQuestion = (double)100 / questionsCount;

            var totalScored = 0.0d;
            foreach (var item in answerByGroup)
            {
                var totalRightQuestions = item.Count(x => x.IsRight);

                var rightCount = item.Count(x => x.IsRight && x.IsSelected);
                var wrongCount = item.Count(x => !x.IsRight && x.IsSelected);

                var scoredAnswers = rightCount - wrongCount;
                if (scoredAnswers > 0)
                {
                    var percentForAnswer = percentForQuestion / totalRightQuestions;
                    totalScored += percentForAnswer * scoredAnswers;
                }
            }

            return (int)totalScored;
        }
    }
}
