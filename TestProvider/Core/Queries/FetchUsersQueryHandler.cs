﻿using Common.Entities;
using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTestProvider.Queries
{
    public class FetchUsersFilterQuery : IQuery
    {
        public int Count { get; set; }
    }
    public class FetchUsersQueryHandler : IQueryHandler<FetchUsersFilterQuery, IEnumerable<User>>
    {
        IUnitOfWork _unitOfWork;
        public FetchUsersQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<User>> Handle(FetchUsersFilterQuery query)
        {
            var users = await _unitOfWork.UserRepository.FetchAllAsync();
            return users.Take(query.Count);
        }
	}
}
