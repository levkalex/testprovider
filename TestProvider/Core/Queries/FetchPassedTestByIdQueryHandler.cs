﻿using Common.Entities;
using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTestProvider.Queries
{
    public class PassedTestByIdFilterQuery : IQuery
    {
        public int Id { get; set; }
    }

    public class FetchPassedTestByIdQueryHandler : IQueryHandler<PassedTestByIdFilterQuery, PassedTest>
    {
        IUnitOfWork _unitOfWork;
        public FetchPassedTestByIdQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<PassedTest> Handle(PassedTestByIdFilterQuery query)
        {
            return _unitOfWork.PassedTestRepository.FetchByIdAsync(query.Id);
        }
    }

}
