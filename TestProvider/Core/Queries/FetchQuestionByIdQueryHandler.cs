﻿using Common.Entities;
using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTestProvider.Queries
{
    public class QuestionByIdFilterQuery : IQuery
    {
        public int Id { get; set; }
    }
    class FetchQuestionByIdQueryHandler : IQueryHandler<QuestionByIdFilterQuery, Question>
    {
        IUnitOfWork _unitOfWork;
        public FetchQuestionByIdQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Question> Handle(QuestionByIdFilterQuery query)
        {
            var question = await _unitOfWork.QuestionRepository.FetchByIdAsync(query.Id);
            return question;
        }
    }
}
