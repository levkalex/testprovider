﻿using Common.Entities;
using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTestProvider.Queries
{
    public class FetchQuestionsFilterQuery : IQuery
    {
        public int Count { get; set; }
        public bool IsEnabled { get; set; }
    }
    public class FetchQuestionsQueryHandler : IQueryHandler<FetchQuestionsFilterQuery, IEnumerable<Question>>
    {
        IUnitOfWork _unitOfWork;
        public FetchQuestionsQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Question>> Handle(FetchQuestionsFilterQuery query)
        {
            var questions = await _unitOfWork.QuestionRepository.FetchAllAsync(query.IsEnabled);
            return questions.Take(query.Count); 
        }
    }
}
