﻿using Common.Entities;
using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTestProvider.Queries
{
    public class UserByIdFilterQuery : IQuery
    {
        public int Id { get; set; }
    }
    class FetchUserByIdQueryHandler : IQueryHandler<UserByIdFilterQuery, User>
    {
        IUnitOfWork _unitOfWork;
        public FetchUserByIdQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<User> Handle(UserByIdFilterQuery query)
        {
            var user = await _unitOfWork.UserRepository.FetchByIdAsync(query.Id);
            return user;
        }
    }
}
