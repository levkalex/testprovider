﻿using Common.Entities;
using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreTestProvider.Queries
{
    public class PassedTestsFilterQuery : IQuery
    {
        public int UserId { get; set; }
        public int Count { get; set; }
    }

    public class FetchPassedTestsQueryHandler : IQueryHandler<PassedTestsFilterQuery, IEnumerable<PassedTest>>
    {
        IUnitOfWork _unitOfWork;
        public FetchPassedTestsQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<IEnumerable<PassedTest>> Handle(PassedTestsFilterQuery query)
        {
            return _unitOfWork.PassedTestRepository.FetchAllAsync(query.UserId, query.Count);
        }
    }

}
