﻿using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Common.Models;

namespace CoreTestProvider.Queries
{
    public class FetchFullPassedTestByIdFilterQuery : IQuery
    {
        public int PassedTestId { get; set; }
    }
    
    public class FetchFullPassedTestByIdQueryHandler : IQueryHandler<FetchFullPassedTestByIdFilterQuery, IEnumerable<PassedTestQuestionModel>>
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public FetchFullPassedTestByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        //TODO: Replace to StoredProcedure!
        public async Task<IEnumerable<PassedTestQuestionModel>> Handle(FetchFullPassedTestByIdFilterQuery query)
        {
            var testQuestionsForPassed = await _unitOfWork.TestQuestionRepository.FetchFullByPassedTestIdAsync(query.PassedTestId);
            var questionsForPassedTest = testQuestionsForPassed.Select(x => x.Question);

            var testAnswerForPassed = await _unitOfWork.TestAnswerRepository.FetchByPassedTestIdAsync(query.PassedTestId);
            var testAnswerIdsForPassed = testAnswerForPassed.Select(x => x.AnswerId);

            var result = _mapper.Map<IEnumerable<PassedTestQuestionModel>>(questionsForPassedTest);
            foreach (var answer in result.SelectMany(x=>x.Answers))
            {
                if (testAnswerIdsForPassed.Contains(answer.Id))
                    answer.IsSelected = true;
            }

            return result;
        }
    }

}
