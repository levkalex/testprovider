﻿using Common.Entities;
using Common.Enums;
using Common.Utils.Extensions;
using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTestProvider.Queries
{
    public class FetchQuestionsWithAnswersFilterQuery : IQuery
    {
        public int Count { get; set; }
        public bool IsEnabled { get; set; }
        public OrderType OrderType { get; set; }
    }

  

    public class FetchQuestionsWithAnswersQueryHandler : IQueryHandler<FetchQuestionsWithAnswersFilterQuery, IEnumerable<Question>>
    {
        IUnitOfWork _unitOfWork;
        public FetchQuestionsWithAnswersQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Question>> Handle(FetchQuestionsWithAnswersFilterQuery query)
        {
            var questions = await _unitOfWork.QuestionRepository.FetchAllWithAnswersAsync(query.IsEnabled);
            questions.Shuffle();
            return questions.Take(query.Count);
        }
    }
}
