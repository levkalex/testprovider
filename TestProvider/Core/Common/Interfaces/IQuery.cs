﻿using System.Threading.Tasks;

namespace CoreTestProvider.Common.Interfaces
{
    public interface IQuery
    {
    }

    public interface IQueryHandler<in TQuery, TResponse> where TQuery : IQuery
    {
        public Task<TResponse> Handle(TQuery query);
    }
}
