﻿using System.Threading.Tasks;

namespace CoreTestProvider.Common.Interfaces
{
    public interface ICommand
    {
    }

    public interface ICommandHandler<in TCommand, TResponse> where TCommand : ICommand
    {
        public Task<TResponse> Handle(TCommand command);
    }

    public interface ICommandHandler<in TCommand> where TCommand : ICommand
    {
        public Task Handle(TCommand command);
    }
}
