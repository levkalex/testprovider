﻿using Common.Constants;
using Common.Entities;
using Common.Exceptions;
using Common.Utils;
using CredentialManagement;
using Database.Common;
using Microsoft.Extensions.Configuration;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace CoreTestProvider.Common
{
    public interface IUserContext
    {
        public int Id { get; }
        public string Name { get; }
        Task SignInAsync(string login, SecureString password, bool saveCredential = true);
        Task SignOutAsync(bool removeCredential = true);
        Task<Credential> FetchCredentialAsync();
    }

    public class UserContext : IUserContext
    {
        public int Id => _user.Id;
        public string Name => _user.Name;
        public bool IsAuthorization { get; private set; }

        private User _user;
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public UserContext(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        public async Task SignInAsync(string login, SecureString password, bool saveCredential = true)
        {
            var hashPassword = HashManager.HashSecureString(password, (byte[] bytes) => HashManager.GenerateSaltedHash(bytes, Encoding.ASCII.GetBytes(login)));

            var user = await _unitOfWork.UserRepository.FetchByUserNamePassAsync(login, hashPassword);
            if (user != null)
            {
                _user = user;
                IsAuthorization = true;

                if (saveCredential)
                {
                    await Task.Run(() =>
                    {
                        new Credential
                        {
                            Target = _configuration[AppSettingKeys.APPLICATION_NAME],
                            Username = login,
                            SecurePassword = password,
                            PersistanceType = PersistanceType.LocalComputer
                        }.Save();
                    });
                }
            }
            else
            {
                throw new AuthenticationException("Invalid password or login");
            }
        }

        public async Task SignOutAsync(bool removeCredential = true)
        {
            IsAuthorization = false;
            _user = null;

            await Task.Run(() =>
            {
                var credential = new Credential { Target = _configuration[AppSettingKeys.APPLICATION_NAME] };
                if (credential.Exists())
                {
                    credential.Delete();
                }
            });
        }

        public Task<Credential> FetchCredentialAsync()
        {
           return Task.Run(() =>
            {
                var credential = new Credential { Target = _configuration[AppSettingKeys.APPLICATION_NAME] };
                if (credential.Exists())
                {
                    credential.Load();
                    return credential;
                }
                return null;
            });
        }
    }
}
