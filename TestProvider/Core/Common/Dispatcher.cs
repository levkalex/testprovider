﻿using Autofac;
using Autofac.Core;
using CoreTestProvider.Common.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTestProvider.Common
{
    public interface IDispatcher
    {
        Task<TResult> Dispatch<TResult>(IQuery query);
        Task<TResult> Dispatch<TResult>(ICommand command);
        Task Dispatch(ICommand command);
    }

    public class Dispatcher : IDispatcher
    {
        ILifetimeScope _scope;
        public Dispatcher(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public Task<TResult> Dispatch<TResult>(IQuery query)
        {
            return (Task<TResult>)ResolveDispatch<TResult>(typeof(IQueryHandler<,>), query.GetType(), query);
        }

        public Task<TResult> Dispatch<TResult>(ICommand command)
        {
            return (Task<TResult>)ResolveDispatch<TResult>(typeof(ICommandHandler<,>), command.GetType(), command);
        }

        private object ResolveDispatch<TResult>(Type handlerType, Type requestType, params object[] obj)
        {
            var genericCommandType = handlerType.MakeGenericType(requestType, typeof(TResult));
            var typeHandler = _scope.ComponentRegistry.Registrations.Single(x => x.Services
                    .OfType<IServiceWithType>()
                    .Any(x => genericCommandType.IsAssignableFrom(x.ServiceType)))
                    .Activator
                    .LimitType;

            var handler = _scope.Resolve(typeHandler);
            var handleMethod = handler.GetType().GetMethod("Handle");
            return handleMethod.Invoke(handler, obj);
        }

        public Task Dispatch(ICommand command)
        {
            var genericCommandType = typeof(ICommandHandler<>).MakeGenericType(command.GetType());
            var typeHandler = _scope.ComponentRegistry.Registrations.Single(x => x.Services
                    .OfType<IServiceWithType>()
                    .Any(x => genericCommandType.IsAssignableFrom(x.ServiceType)))
                    .Activator
                    .LimitType;

            var handler = _scope.Resolve(typeHandler);
            var handleMethod = handler.GetType().GetMethod("Handle");
            var result = handleMethod.Invoke(handler, new object[] { command });

            return (Task)result;
        }
    }
}
