﻿using System;
using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Threading.Tasks;
using Common.Entities;
using System.Linq;
using Common.Models;
using CoreTestProvider.Utils;
using CoreTestProvider.Common;

namespace CoreTestProvider.Commands
{
    public class FinishTestCommand : ICommand
    {
        public int PassedTestId { get; set; }
        public PassedTestAnswerModel[] PassedTestAnswers { get; set; }
    }

    public class FinishTestCommandHandler : ICommandHandler<FinishTestCommand, int>
    {
        IUnitOfWork _unitOfWork;
        public FinishTestCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(FinishTestCommand command)
        {
            var testAnswers = command.PassedTestAnswers
                                    .Where(x=>x.IsSelected)
                                    .Select(x => new TestAnswer() {AnswerId = x.Id, PassedTestId = command.PassedTestId});
            
            foreach (var item in testAnswers)
			{
                await _unitOfWork.TestAnswerRepository.AddAsync(item);
            }
            
            var passedTest = await _unitOfWork.PassedTestRepository.FetchByIdAsync(command.PassedTestId);
            passedTest.FinishDateTime = DateTime.Now;
            passedTest.IsFinished = true;
            passedTest.PercentResult = CalculateTestResult.Calculate(command.PassedTestAnswers);
            await _unitOfWork.PassedTestRepository.UpdateAsync(passedTest);
            
            await _unitOfWork.SaveChangesAsync();
            return command.PassedTestId;
        }
    }
}
