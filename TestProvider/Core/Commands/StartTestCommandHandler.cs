﻿using System;
using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Threading.Tasks;
using Common.Entities;
using System.Linq;
using CoreTestProvider.Common;

namespace CoreTestProvider.Commands
{
    public class StartTestCommand : ICommand
    {
        public int[] QuestionIds { get; set; }
    }

    public class StartTestCommandHandler : ICommandHandler<StartTestCommand, int>
    {
        IUnitOfWork _unitOfWork;
        IUserContext _userContext;
        public StartTestCommandHandler(IUnitOfWork unitOfWork, IUserContext userContext)
        {
            _unitOfWork = unitOfWork;
            _userContext = userContext;
        }

        public async Task<int> Handle(StartTestCommand command)
        {
            var passedTest = new PassedTest() { UserId = _userContext.Id, StartDateTime = DateTime.Now };
            var testQuestions =
                command.QuestionIds.Select(x => new TestQuestion() {PassedTest = passedTest, QuestionId = x});
            
            await _unitOfWork.PassedTestRepository.AddAsync(passedTest);
            foreach (var item in testQuestions)
			{
                await _unitOfWork.TestQuestionRepository.AddAsync(item);
            }
            await _unitOfWork.SaveChangesAsync();
            return passedTest.Id;
        }
    }
}
