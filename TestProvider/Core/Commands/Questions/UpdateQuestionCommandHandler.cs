﻿using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Threading.Tasks;
using Common.Entities;

namespace CoreTestProvider.Commands
{
    public class UpdateQuestionCommand : ICommand
    {
        public Question Question { get; set; }
    }

    public class UpdateQuestionCommandHandler : ICommandHandler<UpdateQuestionCommand, int>
    {
        IUnitOfWork _unitOfWork;
        public UpdateQuestionCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(UpdateQuestionCommand command)
        {
            await _unitOfWork.QuestionRepository.UpdateAsync(command.Question);
            await _unitOfWork.SaveChangesAsync();
            return command.Question.Id;
        }
	}
}
