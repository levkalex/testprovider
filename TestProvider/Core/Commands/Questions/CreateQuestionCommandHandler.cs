﻿using Common.Models;
using CoreTestProvider.Common.Interfaces;
using Database.Common;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Common.Entities;

namespace CoreTestProvider.Commands
{
    public class CreateQuestionCommand : Question, ICommand
    {
    }

    public class CreateQuestionCommandHandler : ICommandHandler<CreateQuestionCommand, int>
    {
        IUnitOfWork _unitOfWork;
        public CreateQuestionCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(CreateQuestionCommand command)
        {
            await _unitOfWork.QuestionRepository.AddAsync(command);
            foreach (var item in command.Answers)
            {
                item.Question = command;
                await _unitOfWork.AnswerRepository.AddAsync(item);
            }
            await _unitOfWork.SaveChangesAsync();
            return command.Id;
        }
    }
}
