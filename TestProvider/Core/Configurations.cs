﻿using AutoMapper;
using Common.Entities;
using Common.Models;

namespace CoreTestProvider
{
    public static class Configurations
    {
        public static void AddCoreMapperConfig(this IMapperConfigurationExpression mapperConfiguration)
        {
            mapperConfiguration.CreateMap<Answer, PassedTestAnswerModel>();
            mapperConfiguration.CreateMap<Question, PassedTestQuestionModel>();
        }
    }
}
