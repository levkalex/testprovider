﻿namespace Common.Constants
{
    public static class AppSettingKeys
    {
        public const string APPLICATION_NAME = "ApplicationName";
        public const string COUNT_QUESTION = "CountQuestion";
    }
}
