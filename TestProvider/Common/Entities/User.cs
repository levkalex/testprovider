﻿namespace Common.Entities
{
	public class User
	{
		public int Id { get; set; }
		public int Access { get; set; }
		public string Name { get; set; }
		public byte[] Password { get; set; }
	}
}
