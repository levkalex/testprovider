﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Word
    {
        public int Id { get; set; }
        public string Russian { get; set; }
        public string English { get; set; }
        public byte[] Image { get; set; }
    }
}
