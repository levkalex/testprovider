﻿namespace Common.Entities
{
    public class TestAnswer
    {
        public int Id { get; set; }
        public int PassedTestId { get; set; }
        public virtual PassedTest PassedTest { get; set; }
        public int AnswerId { get; set; }
        public virtual Answer Answer { get; set; }
    }
}
