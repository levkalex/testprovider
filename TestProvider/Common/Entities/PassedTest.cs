﻿using System;
using System.Collections.Generic;

namespace Common.Entities
{
    public class PassedTest
    {
        public int Id { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime FinishDateTime { get; set; }
        public bool IsFinished { get; set; }
        public int PercentResult { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public virtual List<TestQuestion> TestQuestions { get; set; }
        public virtual List<TestAnswer> TestAnswers { get; set; }
    }
}