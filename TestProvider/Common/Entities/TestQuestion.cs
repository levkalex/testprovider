﻿using System.Collections.Generic;

namespace Common.Entities
{
    public class TestQuestion
    {
        public int Id { get; set; }
        public int PassedTestId { get; set; }
        public virtual PassedTest PassedTest { get; set; }
        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
    }
}
