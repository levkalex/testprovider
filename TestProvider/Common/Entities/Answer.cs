﻿namespace Common.Entities
{
    public class Answer
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Text { get; set; }
        public bool IsRight { get; set; }
        public bool IsEnabled { get; set; }
        public Question Question { get; set; }
    }
}