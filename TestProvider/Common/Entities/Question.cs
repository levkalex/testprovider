﻿using System.Collections.Generic;

namespace Common.Entities
{
    public class Question
    {
        public int Id { get; set; }
        public string ShortName { get; set; }
        public string Text { get; set; }
        
        public string Description { get; set; }
        public byte[] Image { get; set; }
        public bool IsEnabled { get; set; }
        public virtual List<Answer> Answers { get; set; }
    }
}