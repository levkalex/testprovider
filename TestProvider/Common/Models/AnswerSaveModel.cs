﻿namespace Common.Models
{
	public class AnswerSaveModel
	{
		public string Answer { get; set; }
		public bool IsRight { get; set; }
	}
}
