﻿using System.Collections.Generic;
using Common.Entities;

namespace Common.Models
{
    public class PassedTestQuestionModel
    {
        public int Id { get; set; }
        public string ShortName { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }
        public bool IsEnabled { get; set; }
        public List<PassedTestAnswerModel> Answers { get; set; }
    }

    public class PassedTestAnswerModel : Answer
    {
        public bool IsSelected { get; set; }
    }
}