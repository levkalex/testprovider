﻿using Common.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.DomainCore.Repositories
{
    public interface IPassedTestRepository : IRepository<PassedTest>
    {
        Task<IEnumerable<PassedTest>> FetchAllAsync(int userId, int count);
    }
}