﻿using Common.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.DomainCore.Repositories
{
	public interface IUserRepository: IRepository<User>
	{
		Task<User> FetchByUserNamePassAsync(string name, byte[] password);
	}
}
