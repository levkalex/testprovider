﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Entities;

namespace Common.DomainCore.Repositories
{
    public interface IAnswerRepository : IRepository<Answer>
    {
        Task<IEnumerable<Answer>> FetchByQuestionIdAsync(int questionId);
    }
}