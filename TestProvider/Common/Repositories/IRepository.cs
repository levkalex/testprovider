﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.DomainCore.Repositories
{
    public interface IRepository<TEntity>
    {
        Task AddAsync(TEntity entity);
        Task RemoveAsync(TEntity entity);
        Task<IEnumerable<TEntity>> FetchAllAsync();
        Task<TEntity> FetchByIdAsync(int id);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
    }
}