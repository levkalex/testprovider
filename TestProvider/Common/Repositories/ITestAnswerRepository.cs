﻿using Common.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.DomainCore.Repositories
{
    public interface ITestAnswerRepository : IRepository<TestAnswer>
    {
        Task<IEnumerable<TestAnswer>> FetchByPassedTestIdAsync(int passedTestId);
    }
}