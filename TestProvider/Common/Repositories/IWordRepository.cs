﻿using Common.Entities;
using System.Threading.Tasks;

namespace Common.DomainCore.Repositories
{
	public interface IWordRepository: IRepository<Word>
	{
	}
}
