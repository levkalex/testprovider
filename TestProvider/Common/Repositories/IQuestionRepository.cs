﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Entities;

namespace Common.DomainCore.Repositories
{
    public interface IQuestionRepository : IRepository<Question>
    {
        Task<IEnumerable<Question>> FetchAllWithAnswersAsync(bool isEnabled);
        Task<IEnumerable<Question>> FetchAllAsync(bool isEnabled);
    }
}