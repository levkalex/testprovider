﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Entities;

namespace Common.DomainCore.Repositories
{
    public interface ITestQuestionRepository : IRepository<TestQuestion>
    {
        Task<IEnumerable<TestQuestion>> FetchFullByPassedTestIdAsync(int passedTestId);
    }
}