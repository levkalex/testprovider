﻿using System;

namespace Common.Exceptions
{
    public class AuthenticationException : Exception
    {
        public AuthenticationException(string str) : base(str)
        {

        }
    }
}
