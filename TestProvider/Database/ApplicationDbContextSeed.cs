﻿using System;
using System.Text;
using System.Threading.Tasks;
using Common.Entities;
using Database.Common;

namespace Database
{
    public static class ApplicationDbContextSeed
    {
        public static async Task SeedQuestionsAsync(this IUnitOfWork unitOfWork)
        {
            //TODO: Update password (use SHA 256 and salt)
            var firstUser = new User() { Access = 0, Name = "Sergey", Password = Encoding.ASCII.GetBytes("12356") };
            var secondUser = new User() { Access = 0, Name = "Alexandr", Password = Encoding.ASCII.GetBytes("12356") };
            var thirdUser = new User() { Access = 1, Name = "Guest", Password = Encoding.ASCII.GetBytes("12356") };
            
            unitOfWork.UserRepository.AddAsync(firstUser);
            unitOfWork.UserRepository.AddAsync(secondUser);
            unitOfWork.UserRepository.AddAsync(thirdUser);

            var question1 = new Question() { ShortName = "Найдите индексатор", Text = "Среди перечисленных конструкций укажите объявление индексатора:"};
            var question2 = new Question() { ShortName = "Инкапсуляция это...", Text = "Для инкапсуляции вы хотите определить автосвойство так, чтобы невозможно было вызвать сеттер за пределами класса, но геттер оставить доступным. В каком из заданных вариантов описано нужное свойство? "};
            var question3 = new Question() { ShortName = "Finalize", Text = "Для чего предназначен метод Finalize?"};
           
            unitOfWork.QuestionRepository.AddAsync(question1);
            unitOfWork.QuestionRepository.AddAsync(question2);
            unitOfWork.QuestionRepository.AddAsync(question3);
            
            var answer1 = new Answer() { Question = question1, Text = "sting GetName () { return “Name”;}", IsRight = false };
            var answer2 = new Answer() { Question = question1, Text = "string Name;", IsRight = false };
            var answer3 = new Answer() { Question = question1, Text = "string Name {get{return “Name”;}}", IsRight = false };
            var answer4 = new Answer() { Question = question1, Text = "string this [int i] {get {return “Name”;}}", IsRight = true };
            var answer5 = new Answer() { Question = question1, Text = "Don't know", IsRight = false };
            
            unitOfWork.AnswerRepository.AddAsync(answer1);
            unitOfWork.AnswerRepository.AddAsync(answer2);
            unitOfWork.AnswerRepository.AddAsync(answer3);
            unitOfWork.AnswerRepository.AddAsync(answer4);
            unitOfWork.AnswerRepository.AddAsync(answer5);
            
            var answer6 = new Answer() { Question = question2, Text = "public int Value { get; }", IsRight = false };
            var answer7 = new Answer() { Question = question2, Text = "public int Value { get; private set; }", IsRight = true };
            var answer8 = new Answer() { Question = question2, Text = "public int Value { get; set; }", IsRight = false };
            var answer9 = new Answer() { Question = question2, Text = "private int Value { public get; set; }", IsRight = false };
            var answer10 = new Answer() { Question = question2, Text = "int Value { public get; private set; }", IsRight = false };
            
            unitOfWork.AnswerRepository.AddAsync(answer6);
            unitOfWork.AnswerRepository.AddAsync(answer7);
            unitOfWork.AnswerRepository.AddAsync(answer8);
            unitOfWork.AnswerRepository.AddAsync(answer9);
            unitOfWork.AnswerRepository.AddAsync(answer10);
            
            var answer11 = new Answer() { Question = question3, Text = "Для детерминированного уничтожения объектов типа", IsRight = false };
            var answer12 = new Answer() { Question = question3, Text = "У Finalize нет особого предназначения", IsRight = false };
            var answer13 = new Answer() { Question = question3, Text = "Finalize - аналог блока finally", IsRight = false };
            var answer14 = new Answer() { Question = question3, Text = "Позволяет объекту выпол. коректную очист. прежде чем сборщик мусора освоб. занимаемую объектом памят", IsRight = true };
            var answer15 = new Answer() { Question = question3, Text = "Я тоже правильный ответ", IsRight = true };

            unitOfWork.AnswerRepository.AddAsync(answer11);
            unitOfWork.AnswerRepository.AddAsync(answer12);
            unitOfWork.AnswerRepository.AddAsync(answer13);
            unitOfWork.AnswerRepository.AddAsync(answer14);
            unitOfWork.AnswerRepository.AddAsync(answer15);
            
            var passedTest1 = new PassedTest()
            {
                User = firstUser, StartDateTime = DateTime.Now.AddDays(-1),
                FinishDateTime = DateTime.Now.AddDays(-1).AddMinutes(5)
            };
            
            unitOfWork.PassedTestRepository.AddAsync(passedTest1);

            var testQuestions1 = new TestQuestion() {Question = question1, PassedTest = passedTest1 };
            var testQuestions2 = new TestQuestion() {Question = question2, PassedTest = passedTest1 };
            var testQuestions3 = new TestQuestion() {Question = question3, PassedTest = passedTest1 };

            unitOfWork.TestQuestionRepository.AddAsync(testQuestions1);
            unitOfWork.TestQuestionRepository.AddAsync(testQuestions2);
            unitOfWork.TestQuestionRepository.AddAsync(testQuestions3);
            
            var testAnswer1 = new TestAnswer() {Answer = answer1, PassedTest = passedTest1 };
            var testAnswer2 = new TestAnswer() {Answer = answer7, PassedTest = passedTest1 };
            var testAnswer3 = new TestAnswer() {Answer = answer8, PassedTest = passedTest1 };
            var testAnswer4 = new TestAnswer() {Answer = answer14, PassedTest = passedTest1 };
            var testAnswer5 = new TestAnswer() {Answer = answer15, PassedTest = passedTest1 };
            
            unitOfWork.TestAnswerRepository.AddAsync(testAnswer1);
            unitOfWork.TestAnswerRepository.AddAsync(testAnswer2);
            unitOfWork.TestAnswerRepository.AddAsync(testAnswer3);
            unitOfWork.TestAnswerRepository.AddAsync(testAnswer4);
            unitOfWork.TestAnswerRepository.AddAsync(testAnswer5);

            await unitOfWork.SaveChangesAsync();
        }
    }
}