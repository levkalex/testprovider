﻿using System.Threading.Tasks;
using Common.DomainCore.Repositories;
using Database.Common;
using Database.Repositories;

namespace Database
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext _dbContext;
        public UnitOfWork(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        private IAnswerRepository _lazyAnswerRepository;
        private IQuestionRepository _lazyQuestionRepository;
        private IPassedTestRepository _lazyPassedTestRepository;
        private ITestAnswerRepository _lazyTestAnswerRepository;
        private ITestQuestionRepository _lazyTestQuestionRepository;
        private IUserRepository _lazyUserRepository;
        private IWordRepository _lazyWordRepository;

        public IAnswerRepository AnswerRepository => _lazyAnswerRepository ??= new AnswerRepository(_dbContext);
        public IQuestionRepository QuestionRepository => _lazyQuestionRepository ??= new QuestionRepository(_dbContext);
        public IPassedTestRepository PassedTestRepository => _lazyPassedTestRepository ??= new PassedTestRepository(_dbContext);
        public ITestAnswerRepository TestAnswerRepository => _lazyTestAnswerRepository ??= new TestAnswerRepository(_dbContext);
        public ITestQuestionRepository TestQuestionRepository => _lazyTestQuestionRepository ??= new TestQuestionRepository(_dbContext);
        public IUserRepository UserRepository => _lazyUserRepository ??= new UserRepository(_dbContext);
        public IWordRepository WordRepository => _lazyWordRepository ??= new WordRepository(_dbContext);

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}