﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class questionhistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DateTime",
                table: "PassedTests",
                newName: "StartDateTime");

            migrationBuilder.AddColumn<DateTime>(
                name: "FinishDateTime",
                table: "PassedTests",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "PassedTests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "TestQuestion",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PassedTestId = table.Column<int>(type: "int", nullable: false),
                    QuestionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestQuestion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TestQuestion_PassedTests_PassedTestId",
                        column: x => x.PassedTestId,
                        principalTable: "PassedTests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TestQuestion_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TestAnswers_AnswerId",
                table: "TestAnswers",
                column: "AnswerId");

            migrationBuilder.CreateIndex(
                name: "IX_PassedTests_UserId",
                table: "PassedTests",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TestQuestion_PassedTestId",
                table: "TestQuestion",
                column: "PassedTestId");

            migrationBuilder.CreateIndex(
                name: "IX_TestQuestion_QuestionId",
                table: "TestQuestion",
                column: "QuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_PassedTests_Users_UserId",
                table: "PassedTests",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TestAnswers_Answers_AnswerId",
                table: "TestAnswers",
                column: "AnswerId",
                principalTable: "Answers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PassedTests_Users_UserId",
                table: "PassedTests");

            migrationBuilder.DropForeignKey(
                name: "FK_TestAnswers_Answers_AnswerId",
                table: "TestAnswers");

            migrationBuilder.DropTable(
                name: "TestQuestion");

            migrationBuilder.DropIndex(
                name: "IX_TestAnswers_AnswerId",
                table: "TestAnswers");

            migrationBuilder.DropIndex(
                name: "IX_PassedTests_UserId",
                table: "PassedTests");

            migrationBuilder.DropColumn(
                name: "FinishDateTime",
                table: "PassedTests");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "PassedTests");

            migrationBuilder.RenameColumn(
                name: "StartDateTime",
                table: "PassedTests",
                newName: "DateTime");
        }
    }
}
