﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class updateforsaveanswer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TestAnswers_TestQuestion_TestQuestionId",
                table: "TestAnswers");

            migrationBuilder.RenameColumn(
                name: "TestQuestionId",
                table: "TestAnswers",
                newName: "PassedTestId");

            migrationBuilder.RenameIndex(
                name: "IX_TestAnswers_TestQuestionId",
                table: "TestAnswers",
                newName: "IX_TestAnswers_PassedTestId");

            migrationBuilder.AddForeignKey(
                name: "FK_TestAnswers_PassedTests_PassedTestId",
                table: "TestAnswers",
                column: "PassedTestId",
                principalTable: "PassedTests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TestAnswers_PassedTests_PassedTestId",
                table: "TestAnswers");

            migrationBuilder.RenameColumn(
                name: "PassedTestId",
                table: "TestAnswers",
                newName: "TestQuestionId");

            migrationBuilder.RenameIndex(
                name: "IX_TestAnswers_PassedTestId",
                table: "TestAnswers",
                newName: "IX_TestAnswers_TestQuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_TestAnswers_TestQuestion_TestQuestionId",
                table: "TestAnswers",
                column: "TestQuestionId",
                principalTable: "TestQuestion",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
