﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.DomainCore.Repositories;
using Common.Entities;
using Database.Common;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class AnswerRepository : CommonRepository<Answer>, IAnswerRepository
    {
        public AnswerRepository(ApplicationDbContext dbContext) : base (dbContext)
        {
        }
        
        public async Task<IEnumerable<Answer>> FetchByQuestionIdAsync(int questionId)
        {
            var answers = await DbSet.Where(x => x.QuestionId == questionId).ToListAsync();
            return answers;
        }
    }
}