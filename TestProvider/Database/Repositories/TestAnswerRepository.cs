﻿using Common.DomainCore.Repositories;
using Common.Entities;
using Database.Common;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class TestAnswerRepository : CommonRepository<TestAnswer>, ITestAnswerRepository
    {
        public TestAnswerRepository(ApplicationDbContext dbContext) : base (dbContext)
        {
        }

        public async Task<IEnumerable<TestAnswer>> FetchByPassedTestIdAsync(int passedTestId)
        {
            var list = await DbSet.Where(x => x.PassedTestId == passedTestId).AsNoTracking().ToListAsync();
            return list.AsEnumerable();
        }
    }
}