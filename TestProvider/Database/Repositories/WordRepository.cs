﻿using System.Linq;
using System.Threading.Tasks;
using Common.DomainCore.Repositories;
using Common.Entities;
using Database.Common;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
	class WordRepository : CommonRepository<Word>, IWordRepository
	{
		public WordRepository(ApplicationDbContext dbContext) : base (dbContext)
		{

		}
	}
}
