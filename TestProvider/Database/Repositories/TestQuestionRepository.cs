﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.DomainCore.Repositories;
using Common.Entities;
using Database.Common;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class TestQuestionRepository : CommonRepository<TestQuestion>, ITestQuestionRepository
    {
        public TestQuestionRepository(ApplicationDbContext dbContext) : base (dbContext)
        {
        }
        
        public async Task<IEnumerable<TestQuestion>> FetchFullByPassedTestIdAsync(int passedTestId)
        {
            var list = await DbSet.Where(x => x.PassedTestId == passedTestId).Include(x => x.Question).ThenInclude(x=>x.Answers).AsNoTracking().ToListAsync();
            return list.AsEnumerable();
        }
    }
}