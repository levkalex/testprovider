﻿using System.Linq;
using System.Threading.Tasks;
using Common.DomainCore.Repositories;
using Common.Entities;
using Database.Common;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
	class UserRepository : CommonRepository<User>, IUserRepository
	{
		public UserRepository(ApplicationDbContext dbContext) : base (dbContext)
		{

		}

		public Task<User> FetchByUserNamePassAsync(string name, byte[] password) =>
			DbSet.Where(x => x.Name == name && x.Password == password).FirstOrDefaultAsync();
	}
}
