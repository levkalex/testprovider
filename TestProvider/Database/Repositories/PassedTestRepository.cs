﻿using Common.DomainCore.Repositories;
using Common.Entities;
using Database.Common;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Database.Repositories
{
    public class PassedTestRepository : CommonRepository<PassedTest>, IPassedTestRepository
    {
        private ApplicationDbContext _dbContext;
        public PassedTestRepository(ApplicationDbContext dbContext) : base (dbContext)
        {
            _dbContext = dbContext;
        }

        /*TODO
         * public void InvokeProcedure()
        {
            _dbContext.Database.CanConnect();
            var userType = _dbContext.Database.ExecuteSqlRaw("dbo.DetailPassedTest @PassedTestId = {0}", 1);
        }*/

        public async Task<IEnumerable<PassedTest>> FetchAllAsync(int userId, int count)
        {
            return await DbSet.Where(x=>x.UserId == userId).Take(count).AsNoTracking().ToListAsync();
        }
    }
}