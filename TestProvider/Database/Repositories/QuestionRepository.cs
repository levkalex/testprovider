﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.DomainCore.Repositories;
using Common.Entities;
using Database.Common;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class QuestionRepository : CommonRepository<Question>, IQuestionRepository
    {
        public QuestionRepository(ApplicationDbContext dbContext) : base (dbContext)
        {}
        
        public async Task<IEnumerable<Question>> FetchAllWithAnswersAsync(bool isEnabled)
        {
            var list = await DbSet.Include(x => x.Answers).Where(y => (y.IsEnabled == isEnabled)).AsNoTracking().ToListAsync();
            return list.AsEnumerable();
        }
        public async Task<IEnumerable<Question>> FetchAllAsync(bool isEnabled) 
        {
            var list = await DbSet.Where(x => (x.IsEnabled == isEnabled)).AsNoTracking().ToListAsync();
            return list.AsEnumerable();
        }
        public override Task<Question> FetchByIdAsync(int id)
        {
            return DbSet.Include(x => x.Answers).AsNoTracking().SingleAsync(x=>x.Id == id);
        }

        public override Task UpdateAsync(Question question)
        {
            DbSet.Update(question);
            DbContext.Entry(question).State = EntityState.Modified;
            return Task.CompletedTask;
        }
    }
}