﻿using System.Threading.Tasks;
using Common.DomainCore.Repositories;

namespace Database.Common
{
    public interface IUnitOfWork
    {
        IAnswerRepository AnswerRepository { get; }
        IQuestionRepository QuestionRepository { get; }
        IPassedTestRepository PassedTestRepository { get; }
        ITestAnswerRepository TestAnswerRepository { get; }
        ITestQuestionRepository TestQuestionRepository { get; }
        IUserRepository UserRepository { get; }
        IWordRepository WordRepository { get; }
        Task SaveChangesAsync();
    }
}