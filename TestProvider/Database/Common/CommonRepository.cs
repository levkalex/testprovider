﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.DomainCore.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Database.Common
{
    public abstract class CommonRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected internal readonly DbContext DbContext;
        protected internal readonly DbSet<TEntity> DbSet;
        
        public CommonRepository(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
            DbSet = dbContext.Set<TEntity>();
        }
        
        public virtual Task AddAsync(TEntity entity)
        {
            DbContext.Entry(entity).State = EntityState.Added;
            DbSet.Add(entity);
            
            return Task.CompletedTask;
        }

        public virtual Task RemoveAsync(TEntity entity)
        {
            if (DbContext.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            DbSet.Remove(entity);

            return Task.CompletedTask;
        }

        public virtual async Task<IEnumerable<TEntity>> FetchAllAsync()
        {
            var list = await DbSet.AsNoTracking().ToListAsync();
            return list.AsEnumerable();
        }

        public virtual async Task<TEntity> FetchByIdAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public virtual Task DeleteAsync(TEntity entityToDelete)
        {
            if (DbContext.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }
            DbSet.Remove(entityToDelete);

            return Task.CompletedTask;
        }
        
        public virtual Task UpdateAsync(TEntity entity)
        {
            DbSet.Attach(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
            return Task.CompletedTask;
        }
    }
}