﻿using Common.Entities;
using Microsoft.EntityFrameworkCore;

namespace Database.Common
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<TestAnswer> TestAnswers { get; set; }
        public DbSet<PassedTest> PassedTests { get; set; }
        public DbSet<TestQuestion> TestQuestion { get; set; }
        public DbSet<Word> Words { get; set; }
    }
}