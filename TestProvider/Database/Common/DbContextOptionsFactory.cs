﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Database.Common
{
    public class DbContextOptionsFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            
            string connectionString = "workstation id=test-provider.mssql.somee.com;packet size=4096;user id=Yoller_SQLLogin_1;pwd=7defbocfxh;data source=test-provider.mssql.somee.com;persist security info=False;initial catalog=test-provider";
            optionsBuilder.UseSqlServer(connectionString);

            return new ApplicationDbContext(optionsBuilder.Options);
        }
    }
}