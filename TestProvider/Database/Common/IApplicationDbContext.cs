﻿using Common.Entities;
using Microsoft.EntityFrameworkCore;

namespace Database.Common
{
    public interface IApplicationDbContext
    {
        DbSet<Question> Questions { get; set; }
        DbSet<Answer> Answers { get; set; }
    }
}